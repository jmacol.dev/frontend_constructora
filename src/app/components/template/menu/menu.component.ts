import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { SesionService } from 'src/app/services/sesion.service';
import { SidenavService } from 'src/app/services/sidenav.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.sass']
})
export class MenuComponent implements OnInit {
  @Output() openNav = new EventEmitter();
  showItem:boolean = false
  panelOpenState = false;
  constructor(
    public sidenavservc: SidenavService,
    public sserv: SesionService,
    public segurserv: SeguridadService,
    public configServ: ConfiguracionService,
    public properties: PropertiesService
  ) {

  }

  ngOnInit(): void {
    this.sidenavservc.iconmenu = true
    let usu: Usuario = JSON.parse(this.sserv.getuser(localStorage.getItem('user')??'{}') ?? '{}')
    this.segurserv.usuarioreg = usu
    // this.segurserv.usuarioreg.perfil = usu.perfil == '' ? 'Conectado' : usu.perfil
    this.showItem = (this.properties.id_admin == usu.n_idusuario)
  }


}
