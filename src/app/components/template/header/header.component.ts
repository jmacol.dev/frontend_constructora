import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import { CambiarpassComponent } from '../../auth/cambiarpass/cambiarpass.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  @Output() openNav = new EventEmitter();

  public icon: boolean = true;
  constructor(
    public sidenavservc: SidenavService,
    public segurserv: SeguridadService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  cambiopass(){
    const dialogRef = this.dialog.open(CambiarpassComponent, {
      width: '350px',
      panelClass: 'divmodal',
      data: {title: 'Cambiar contraseña'},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
    });
  }
}
