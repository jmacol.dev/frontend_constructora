import { Component,  EventEmitter,  Input,  OnDestroy,  OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/helpers/format-datepicker';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { NavigationEnd, Router } from '@angular/router';
import { OutEventService } from 'src/app/services/out-event.service';


@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]

})
export class ButtonComponent implements OnInit, OnDestroy {
  navigationSubscription;
  @Output() openNvo = new EventEmitter();
  @Output() openList = new EventEmitter();
  @Output() openListSC = new EventEmitter();
  @Input() btnCtrl: any = [false, false, false, false, false];
  @Input() opcion: number = 0;
  @Input() modulo: string = '';

  banlista: boolean = this.btnCtrl[0];
  bannuevo: boolean = this.btnCtrl[1];
  banexportar: boolean = this.btnCtrl[2];
  bancarga: boolean = this.btnCtrl[3];
  banregresa: boolean = this.btnCtrl[4];
  campaignOne: FormGroup = new FormGroup({});
  today: Date | undefined;
  datepast: Date | undefined;
  classanc: String = 'col-lg-12';
  date: any;

  panelOpenState = false;
  public anio: number = new Date().getFullYear();
  constructor(
    public configservc: ConfiguracionService,
    public outserv: OutEventService,
    private router: Router
  ) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // Si es un evento NavigationEnd reinicialice el componente
      if (e instanceof NavigationEnd) {
        this.initialiseInvites();
      }
    });
  }


  initialiseInvites() {
    // Establezca valores predeterminados y vuelva a obtener los datos que necesite.

  }

  ngOnInit(): void {
    this.initialiseInvites();
    this.bannuevo = this.btnCtrl[0];
    this.banlista = this.btnCtrl[1];
    this.banexportar = false //this.btnCtrl[2];
    this.bancarga = this.btnCtrl[3];
    this.banregresa = this.btnCtrl[4];
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  listEvent(e: any) {
    this.outserv.listar(this.openList.emit(e))
  }

  excelEvent(e: any) {
    this.outserv.excel(this.openList.emit(e))
  }

  pdfEvent(e: any) {
    this.outserv.pdf(this.openList.emit(e))
  }

  newEvent(e: any) {
    this.outserv.nuevo(this.openNvo.emit(e));
  }

  cargaEvent(e: any) {
    this.outserv.carga(this.openNvo.emit(e));
  }


  regresarEvent(e: any) {
    this.outserv.regresar(this.openNvo.emit(e));
  }


}
