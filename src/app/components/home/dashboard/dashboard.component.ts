import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { SidenavService } from 'src/app/services/sidenav.service';

import * as Highcharts from 'highcharts';
import Exporting from 'highcharts/modules/exporting';
import OfflineExporting from 'highcharts/modules/offline-exporting';
import ExportData from 'highcharts/modules/export-data';
import HighchartsMore from 'highcharts/highcharts-more';
Exporting(Highcharts);
OfflineExporting(Highcharts);
ExportData(Highcharts);
HighchartsMore(Highcharts);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})

export class DashboardComponent implements OnInit {
  Highcharts: typeof Highcharts = Highcharts;
  @ViewChild('drawer') sideNav: any;
  ipAddress: string = '0.0.0.0'
  constructor(
    public sidenavservc: SidenavService,
    public segserv: SeguridadService,
    private router: Router,
    public configserv: ConfiguracionService,

  ) {

    this.segserv.disload = false;
   }



  ngOnInit(): void {


  }

  ngAfterViewInit(): any {
    this.sidenavservc.abrirmenu(this.sideNav);
  }


}
