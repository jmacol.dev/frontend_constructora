import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { NgxSpinnerService } from 'ngx-spinner';
import { Constante } from 'src/app/models/constante';
import { ApisService } from 'src/app/services/apis.service';
import { CloseModalService } from 'src/app/services/close-modal.service';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { ModalService } from 'src/app/services/modal.service';
import { OutEventService } from 'src/app/services/out-event.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import { ValidateService } from 'src/app/services/validate.service';
import Swal from 'sweetalert2';
import { ConstanteMantenimientoComponent } from '../constante-mantenimiento/constante-mantenimiento.component';
import { PaginationModalService } from 'src/app/services/pagination-modal.service';

@Component({
  selector: 'app-constante-detalle',
  templateUrl: './constante-detalle.component.html',
  styleUrls: ['./constante-detalle.component.sass']
})
export class ConstanteDetalleComponent implements OnInit {
  public constante_regis: Constante = new Constante()
  public dataGlobalDC: Constante[] = []
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent();
  search:string = ""
  constructor(
    public outServ: OutEventService,
    public apiServ: ApisService,
    public properties: PropertiesService,
    public pageServ: PaginationModalService,
    public sidenavServ: SidenavService,
    public spinner: NgxSpinnerService,
    public dialogRef: MatDialogRef<ConstanteDetalleComponent>,
     public dialog: MatDialog,
    private closeModal:CloseModalService,
    @Inject(MAT_DIALOG_DATA) public data: {title: '', id: 0}
  ) {
  }

  ngOnInit(): void {
    this.lista_detalleConstantes()

  }

  ngAfterViewInit(): any {
    this.outServ.nuevo = () => {
      console.log("nuevo")
      this.editar(0,this.data.id)
     };

     this.outServ.listarModal = () => {
      this.lista_detalleConstantes()
      console.log("listar ==> " )
    }
   }

  lista_detalleConstantes() {
    this.spinner.show();
    this.pageEvent = this.pageServ.inicializarModal();
    this.apiServ.listado(`${this.apiServ.api_constante}list`, `?n_idsuperior=${this.data.id}` ).then(response => {
      console.log(JSON.stringify(response));
      this.pageEvent = this.pageServ.inicializarModal()
      if (response.length == 0) {
        this. pageServ.dataSourceModal = []
      }else {
        console.log("pasa")
        this.dataGlobalDC = response
        this.rediseTablaPage(response);
      }
    }).finally(() => {
      this.spinner.hide();
    })
  }

  applyFilter() {
    this.search = this.search.toLowerCase(); // Datasource defaults to lowercase matches
    let data = this.dataGlobalDC.filter(item => (item.v_nombre).toLowerCase().includes(this.search) || (item.v_descripcion).toLowerCase().includes(this.search));
    this.rediseTablaPage(data);
  }

  rediseTablaPage(data: any) {
      this.pageServ.coleccionsize = this.pageServ.dataSourceModal.length
      this.pageServ.dataSourceModal = data
      this.collectionSize = this.pageServ.dataSourceModal.length
      this.pageEvent.length = data.length
      this.pageServ.actualizaTablaModal(data, this.pageEvent);
  }

  editar($id: number, $superior:number) {
    // this.modalServ.constanteModal($id, $superior);
    let $title: string = $superior == 0 ? ($id == 0 ? this.properties.constante.registrotipo : this.properties.constante.editartipo)
      :($id == 0 ? this.properties.constante.registro : this.properties.constante.editar)
    const dialogRef = this.dialog.open(ConstanteMantenimientoComponent, {
      width: '500px',
      panelClass: 'divmodal',
      data: { title: $title, id:$id, superior:this.data.id},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
      this.outServ.listarModal(null)
    });
  }

  estado(id: number, estado: any) {

    let $request = {
      n_idconstante: id,
      b_vigencia: estado
    }
    console.log(estado)

    Swal.fire({
          title: this.data.title,
          text: estado == 0 ? this.properties.confirminhab : this.properties.confirmactiv,
          icon: 'question',
          showCancelButton: true,
          confirmButtonText: 'Aceptar',
          cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.apiServ.estado(this.data.title, `${this.apiServ.api_constante}delete`, $request).then(() => {
          this.outServ.listarModal(null)
        })
      }
    })
  }

  guardar() { }

  cancelar() {
    this.closeModal.cancelarModal(this.data.title, this.dialogRef)
  }

}
