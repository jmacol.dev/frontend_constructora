import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstanteDetalleComponent } from './constante-detalle.component';

describe('ConstanteDetalleComponent', () => {
  let component: ConstanteDetalleComponent;
  let fixture: ComponentFixture<ConstanteDetalleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConstanteDetalleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstanteDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
