import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrabajadorMantenimientoComponent } from './trabajador-mantenimiento.component';

describe('TrabajadorMantenimientoComponent', () => {
  let component: TrabajadorMantenimientoComponent;
  let fixture: ComponentFixture<TrabajadorMantenimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrabajadorMantenimientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrabajadorMantenimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
