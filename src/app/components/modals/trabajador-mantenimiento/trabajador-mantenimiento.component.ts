import { FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Trabajador } from 'src/app/models/trabajador';
import { PropertiesService } from 'src/app/services/properties.service';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ValidateService } from 'src/app/services/validate.service';
import { ApisService } from 'src/app/services/apis.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { APP_DATE_FORMATS, AppDateAdapter } from 'src/app/helpers/format-datepicker';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { ComboService } from 'src/app/services/combo.service';
import { Constante, ConstanteCombo } from 'src/app/models/constante';
import { MatCheckbox, MatCheckboxChange } from '@angular/material/checkbox';
import { NgxSpinnerService } from 'ngx-spinner';
import { PaginationService } from 'src/app/services/pagination.service';
import { MatSelect } from '@angular/material/select';
import { CloseModalService } from 'src/app/services/close-modal.service';

@Component({
  selector: 'app-trabajador-mantenimiento',
  templateUrl: './trabajador-mantenimiento.component.html',
  styleUrls: ['./trabajador-mantenimiento.component.sass']
})
export class TrabajadorMantenimientoComponent implements OnInit {
  @ViewChild('chkUsu') chkUsu: any
  usuario_g: string = ""
  chkUsuario:boolean = false
  trabajador_regis: Trabajador = new Trabajador();
  date: any;
  filteredOptions!: Observable<Trabajador[]>;
  myControl = new FormControl();
  options: Trabajador[] = []
  comboTipoDoc: ConstanteCombo[] = [];
  constructor(
    private closeModal:CloseModalService,
    public validaServ: ValidateService,
    private properties: PropertiesService,
    public apiServ: ApisService,
    protected comboServ: ComboService,
    public dialogRef: MatDialogRef<TrabajadorMantenimientoComponent>,
    private spinner: NgxSpinnerService,
    public pageServ: PaginationService,
    @Inject(MAT_DIALOG_DATA) public data: {title: '', id: 0}
  ) {
    this.trabajador_regis.n_idtrabajador = data.id
    this.date = new FormControl(new Date());
    this.comboServ.combo_constante(this.properties.tipoconstante.tipodocumento).then(dataCombo => {
      this.comboTipoDoc = dataCombo;
    })
   }

  ngOnInit(): void {
    console.log(this.data.id)
    if (this.data.id > 0) {
      this.obtener_trabajador()
    }
    if (this.chkUsu != undefined) this.chkUsu.checked = false
  }

  obtener_trabajador() {
  this.apiServ.listado(`${this.apiServ.api_trabajador}get`, `?n_idtrabajador=${this.data.id}` ).then(response => {
      console.log(JSON.stringify(response));
      this.trabajador_regis = response;
      this.selectOnChange(this.trabajador_regis)

    })
  }

  handleModelChange(e:any){
    if(e == ''){
      this.trabajador_regis.v_numdoc =  ''
      this.trabajador_regis.v_nombres =  ''
      this.trabajador_regis.v_apellidos =  ''
      this.trabajador_regis.v_telefono =  ''
      this.trabajador_regis.v_email =  ''
    }
  }

  generaUsuario() {
    if (this.chkUsu != undefined) {
      if (this.chkUsu.checked) {
        if (this.validaServ.validaGeneraUsuario(this.trabajador_regis)) {
          this.apiServ.listado(`usuario/count-user`, `?v_nombres=${this.trabajador_regis.v_nombres}&v_apellidos=${this.trabajador_regis.v_apellidos}`).then(response => {
            console.log(JSON.stringify(response));
            this.usuario_g = response;
          })
        } else {
          console.log('no valids')
          this.chkUsu.checked = false
        }
      }
    }
  }


  selectOnChange(obj: Trabajador) {
    let tipodoc:any  = obj.n_idtipodoc.toString()
    this.trabajador_regis.n_idtipodoc =  tipodoc
    console.log("tipodocuemnto ===> " + tipodoc)
  }

  guardar() {
    this.spinner.show();
    console.log(JSON.stringify(this.trabajador_regis))
    let tipoapi = this.trabajador_regis.n_idtrabajador> 0 ? 'update' : 'insert'
    if (this.validaServ.validaTrabajador(this.trabajador_regis)) {
      if (this.trabajador_regis.n_idtrabajador > 0) {
        this.apiServ.actualizar(this.data.title, `${this.apiServ.api_trabajador}${tipoapi}`, this.trabajador_regis).then((response) => {
          this.spinner.hide();
           if (response)  this.closeModal.onNoClickDialog(this.dialogRef, 1)
        })
      } else {
        let usu_gen = null
        if (this.chkUsu.checked) {
          usu_gen = {
            usuario: this.usuario_g,
            a_v_ipcrea: '0.0.0.0',
            b_vigencia: 1
          }
        }
        let request = {
          n_idtrabajador: this.trabajador_regis.n_idtrabajador,
          v_numdoc: this.trabajador_regis.v_numdoc,
          v_nombres: this.trabajador_regis.v_nombres,
          v_apellidos: this.trabajador_regis.v_apellidos,
          v_telefono: this.trabajador_regis.v_telefono,
          v_email: this.trabajador_regis.v_email,
          n_idtipodoc: this.trabajador_regis.n_idtipodoc,
          b_vigencia: 1,
          a_v_ipcrea: this.trabajador_regis.a_v_ipcrea,
          usuario:usu_gen
        }

        this.apiServ.registro(this.data.title, `${this.apiServ.api_trabajador}${tipoapi}`, request).then((response) => {
          this.spinner.hide();
           if (response)  this.closeModal.onNoClickDialog(this.dialogRef, 1)
        })
      }

    }
  }

  cancelar() {
    this.closeModal.cancelarModal(this.properties.datos_route.trabajador.modulo, this.dialogRef)
  }

}
