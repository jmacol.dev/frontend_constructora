import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PruebaMantenimientoComponent } from './prueba-mantenimiento.component';

describe('PruebaMantenimientoComponent', () => {
  let component: PruebaMantenimientoComponent;
  let fixture: ComponentFixture<PruebaMantenimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PruebaMantenimientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PruebaMantenimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
