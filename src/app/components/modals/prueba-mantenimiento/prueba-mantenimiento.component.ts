import { FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Prueba } from 'src/app/models/prueba';
import { PropertiesService } from 'src/app/services/properties.service';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ValidateService } from 'src/app/services/validate.service';
import { ApisService } from 'src/app/services/apis.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { APP_DATE_FORMATS, AppDateAdapter } from 'src/app/helpers/format-datepicker';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { CloseModalService } from 'src/app/services/close-modal.service';
import { DateService } from 'src/app/services/date.service';
import { ComboService } from 'src/app/services/combo.service';

@Component({
  selector: 'app-prueba-mantenimiento',
  templateUrl: './prueba-mantenimiento.component.html',
  styleUrls: ['./prueba-mantenimiento.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class PruebaMantenimientoComponent implements OnInit {

  prueba_regis: Prueba = new Prueba();
  date: any;
  filteredOptions!: Observable<Prueba[]>;
  myControl = new FormControl();
  options: Prueba[] = []
  constructor(
    public validaServ: ValidateService,
    private properties: PropertiesService,
    public apiServ: ApisService,
    public dialogRef: MatDialogRef<PruebaMantenimientoComponent>,
    private closeModal: CloseModalService,
    public comboServ: ComboService,
    public dateServ: DateService,
    @Inject(MAT_DIALOG_DATA) public data: {title: '', id: 0}
  ) {
    this.prueba_regis.n_id = data.id
    this.date = new FormControl(new Date());
    this.lista_autocomplete()
   }

  ngOnInit(): void {
    if (this.data.id > 0) {
      this.obtener_prueba()
    }
  }

  obtener_prueba() {
    this.apiServ.listado(`${this.apiServ.api_prueba}get?`, `n_id=${this.data.id}` ).then(response => {
      console.log(JSON.stringify(response));

      this.prueba_regis = response;
      this.prueba_regis.d_fecha = this.dateServ.fecha_act(this.prueba_regis.d_fecha)
      this.selectOnChange(this.prueba_regis)
      this.myControl.setValue(this.options.filter(option => option.n_id.toString().toLowerCase().includes(this.prueba_regis.n_autocomplete.toString()))[0].v_cadena)

    })
  }

  handleModelChange(e:any){
    if(e == ''){
      this.prueba_regis.v_cadena =  ''
    }
  }

  lista_autocomplete(){
    this.apiServ.listado(`${this.apiServ.api_prueba}list?`,'').then(response=>{
      let pruebaList: Prueba[] = response;
      this.options  = pruebaList.sort();
      this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.v_cadena),
        map(v_cadena => v_cadena ? this._filter(v_cadena) : this.options.slice())
      );
    })
  }


  displayFn(obj: Prueba): string {
    return obj && obj.v_cadena ? obj.v_cadena : '';
  }

  private _filter(search: string): Prueba[] {
    const filterValue = search.toLowerCase();

    return this.options.filter(option => option.v_cadena.toLowerCase().includes(filterValue) );
  }

  selectOnChange(obj: Prueba) {
    let combo:any = obj.n_combo.toString()
      this.prueba_regis.n_combo = combo
  }

  autoCompleteOnChange($id: any) {
    console.log("autocomplete ===> " + $id)
      this.prueba_regis.n_autocomplete = $id
  }

  guardar() {
    console.log(JSON.stringify(this.prueba_regis))
    let tipoapi = this.prueba_regis.n_id > 0 ? 'update' : 'insert'
    if (this.validaServ.validaPrueba(this.prueba_regis)) {
      if (this.prueba_regis.n_id > 0) {
        this.apiServ.actualizar(this.data.title, `${this.apiServ.api_prueba}${tipoapi}`, this.prueba_regis).then((response) => {
           if (response)  this.closeModal.onNoClickDialog(this.dialogRef, 1)
        })
      } else {
        this.apiServ.registro(this.data.title, `${this.apiServ.api_prueba}${tipoapi}`, this.prueba_regis).then((response) => {
           if (response)  this.closeModal.onNoClickDialog(this.dialogRef, 1)
        })
      }

    }
  }

  cancelar() {
    this.closeModal.cancelarModal(this.properties.datos_route.prueba.modulo, this.dialogRef)
  }

}
