import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { APP_DATE_FORMATS, AppDateAdapter } from 'src/app/helpers/format-datepicker';
import { ConstanteCombo } from 'src/app/models/constante';
import { DetalleAsignacion } from 'src/app/models/detalle-asignacion';
import { ResultadoAsignacion } from 'src/app/models/resultado-asignacion';
import { ApisService } from 'src/app/services/apis.service';
import { CloseModalService } from 'src/app/services/close-modal.service';
import { ComboService } from 'src/app/services/combo.service';
import { DateService } from 'src/app/services/date.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { ValidateService } from 'src/app/services/validate.service';

@Component({
  selector: 'app-resultado-asignacion',
  templateUrl: './resultado-asignacion.component.html',
  styleUrls: ['./resultado-asignacion.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class ResultadoAsignacionComponent implements OnInit {

  resultado_regis: ResultadoAsignacion = new ResultadoAsignacion()
  myControl = new FormControl();
  title: string = "";
  route: string = ""
  modulo: string = '';
  date: any;
  objsel: DetalleAsignacion = new DetalleAsignacion();
  public comboResultado: ConstanteCombo[] = [];
  constructor(
    public validaServ: ValidateService,
    private properties: PropertiesService,
    public apiServ: ApisService,
    public dialogRef: MatDialogRef<ResultadoAsignacionComponent>,
    private closeModal: CloseModalService,
    public comboServ: ComboService,
    public dateServ: DateService,
    @Inject(MAT_DIALOG_DATA) public data: {title: '', id: 0, data: DetalleAsignacion}
  ) {
    this.title = properties.resultadoasignacion.titulo;
    this.route = properties.datos_route.resultadoasignacion.route;
    this.modulo = properties.datos_route.resultadoasignacion.modulo;
    this.comboServ.combo_constante(this.properties.tipoconstante.resultadoasignacion).then(dataCombo => {
        this.comboResultado = dataCombo;
    })
    this.objsel = data.data;
  }

  ngOnInit(): void {
    this.date = new FormControl(new Date());
    this.resultado_regis.n_idasignadeta = this.data.id
  }

  guardar() {
    this.apiServ.registro(this.data.title, `${this.apiServ.api_resultadoasignacion}insert`, this.resultado_regis).then((response) => {
       if (response)  this.closeModal.onNoClickDialog(this.dialogRef, 1)
    })
  }

  cancelar() {
    this.closeModal.cancelarModal(this.properties.datos_route.resultadoasignacion.modulo, this.dialogRef)
  }

}
