import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadoAsignacionComponent } from './resultado-asignacion.component';

describe('ResultadoAsignacionComponent', () => {
  let component: ResultadoAsignacionComponent;
  let fixture: ComponentFixture<ResultadoAsignacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultadoAsignacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadoAsignacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
