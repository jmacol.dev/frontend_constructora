import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { Cliente } from 'src/app/models/cliente';
import { PropertiesService } from 'src/app/services/properties.service';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ValidateService } from 'src/app/services/validate.service';
import { ApisService } from 'src/app/services/apis.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { APP_DATE_FORMATS, AppDateAdapter } from 'src/app/helpers/format-datepicker';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { ComboService } from 'src/app/services/combo.service';
import { Constante, ConstanteCombo } from 'src/app/models/constante';
import { MatCheckbox, MatCheckboxChange } from '@angular/material/checkbox';
import { NgxSpinnerService } from 'ngx-spinner';
import { PaginationService } from 'src/app/services/pagination.service';
import { MatSelect } from '@angular/material/select';
import { CloseModalService } from 'src/app/services/close-modal.service';


@Component({
  selector: 'app-cliente-mantenimiento',
  templateUrl: './cliente-mantenimiento.component.html',
  styleUrls: ['./cliente-mantenimiento.component.sass']
})
export class ClienteMantenimientoComponent implements OnInit {
  @ViewChild('chkUsu') chkUsu: any
  usuario_g: string = ""
  chkUsuario:boolean = false
  cliente_regis: Cliente = new Cliente();
  date: any;
  filteredOptions!: Observable<Cliente[]>;
  myControl = new FormControl();
  options: Cliente[] = []
  comboEstado: ConstanteCombo[] = [];
  constructor(
    private closeModal:CloseModalService,
    public validaServ: ValidateService,
    private properties: PropertiesService,
    public apiServ: ApisService,
    protected comboServ: ComboService,
    public dialogRef: MatDialogRef<ClienteMantenimientoComponent>,
    private spinner: NgxSpinnerService,
    public pageServ: PaginationService,

    @Inject(MAT_DIALOG_DATA) public data: {title: '', id: 0}
    ) {
      this.cliente_regis.n_idcliente = data.id
      this.date = new FormControl(new Date());
      this.comboServ.combo_constante(this.properties.tipoconstante.estadocliente).then(dataCombo => {
        this.comboEstado = dataCombo;
      })
    }

  ngOnInit(): void {
    if (this.data.id > 0) {
      this.obtener_cliente()
    }
  }

  obtener_cliente() {
    this.apiServ.listado(`${this.apiServ.api_cliente}get`, `?n_idcliente=${this.data.id}` ).then(response => {
        console.log(JSON.stringify(response));
        this.cliente_regis = response;
        this.selectOnChange(this.cliente_regis)

      })
    }

    selectOnChange(obj: Cliente) {
      let tipoEstado:any  = obj.n_idestadocliente.toString()
      this.cliente_regis.n_idestadocliente =  tipoEstado
      console.log("estadoasignacion ===> " + tipoEstado)
    }

    handleModelChange(e:any){
      if(e == ''){
        this.cliente_regis.v_numero =  ''
        this.cliente_regis.v_dni =  ''
        this.cliente_regis.v_nombre =  ''
      }
    }


  guardar() {
    console.log(JSON.stringify(this.cliente_regis))
    let tipoapi = this.cliente_regis.n_idcliente > 0 ? 'update' : 'insert'
    if (this.validaServ.validaCliente(this.cliente_regis)) {
      if (this.cliente_regis.n_idcliente > 0) {
        this.apiServ.actualizar(this.data.title, `${this.apiServ.api_cliente}${tipoapi}`, this.cliente_regis).then((response) => {
          if (response)  this.closeModal.onNoClickDialog(this.dialogRef, 1)
        })
      } else {
        this.apiServ.registro(this.data.title, `${this.apiServ.api_cliente}${tipoapi}`, this.cliente_regis).then((response) => {
           if (response)  this.closeModal.onNoClickDialog(this.dialogRef, 1)
        })
      }

    }
  }

  cancelar() {
    this.closeModal.cancelarModal(this.properties.datos_route.cliente.modulo, this.dialogRef)
  }

}
