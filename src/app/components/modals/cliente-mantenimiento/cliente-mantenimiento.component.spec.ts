import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteMantenimientoComponent } from './cliente-mantenimiento.component';

describe('ClienteMantenimientoComponent', () => {
  let component: ClienteMantenimientoComponent;
  let fixture: ComponentFixture<ClienteMantenimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClienteMantenimientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteMantenimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
