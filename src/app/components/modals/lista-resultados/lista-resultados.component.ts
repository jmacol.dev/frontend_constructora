import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { NgxSpinnerService } from 'ngx-spinner';
import { Cliente } from 'src/app/models/cliente';
import { DetalleAsignacion } from 'src/app/models/detalle-asignacion';
import { ResultadoAsignacion } from 'src/app/models/resultado-asignacion';
import { ApisService } from 'src/app/services/apis.service';
import { CloseModalService } from 'src/app/services/close-modal.service';
import { OutEventService } from 'src/app/services/out-event.service';
import { PaginationModalService } from 'src/app/services/pagination-modal.service';
import { PaginationService } from 'src/app/services/pagination.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { ValidateService } from 'src/app/services/validate.service';

@Component({
  selector: 'app-lista-resultados',
  templateUrl: './lista-resultados.component.html',
  styleUrls: ['./lista-resultados.component.sass']
})
export class ListaResultadosComponent implements OnInit {

   public dataGlobalRes: ResultadoAsignacion[] = []
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;
  title: string = "";
  route: string = ""
  modulo: string = '';
  objsel: Cliente = new Cliente();
  constructor(public validaServ: ValidateService,
    private properties: PropertiesService,
    public apiServ: ApisService,
    public dialogRef: MatDialogRef<ListaResultadosComponent>,
    private closeModal: CloseModalService,
    public outServ: OutEventService,
    public pageServ: PaginationModalService,
    private spinner: NgxSpinnerService,
    @Inject(MAT_DIALOG_DATA) public data: {title: '', id: 0, data: Cliente}
  ) {
    this.title = properties.resultadoasignacion.titulo;
    this.route = properties.datos_route.resultadoasignacion.route;
    this.modulo = properties.datos_route.resultadoasignacion.modulo;
    this.objsel = data.data
    this.pageEvent = this.pageServ.inicializarModal()
    this.rediseTablaPage(this.dataGlobalRes)
  }

  ngOnInit(): void {
    this.listar_resultados()
  }

  listar_resultados() {
    this.pageEvent = this.pageServ.inicializarModal()
    this.spinner.show();
    let request = `?b_vigencia=1&n_idcliente=${this.data.data.n_idcliente}`
    console.log(request)
    this.apiServ.listado(`${this.apiServ.api_resultadoasignacion}list`, request ).then(response => {
      console.log(JSON.stringify(response));
      this.pageServ.dataSourceModal = []
      this.dataGlobalRes = response
      this.rediseTablaPage(response);
    }).finally(() => {
      this.spinner.hide();
    })
  }

  rediseTablaPage(data: any) {
      this.pageServ.coleccionsize = this.pageServ.dataSourceModal.length
      this.pageServ.dataSourceModal = data
      this.collectionSize = this.pageServ.dataSourceModal.length
      this.pageEvent.length = data.length
      this.pageServ.actualizaTablaModal(data, this.pageEvent);
  }

}
