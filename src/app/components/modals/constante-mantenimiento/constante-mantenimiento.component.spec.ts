import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstanteMantenimientoComponent } from './constante-mantenimiento.component';

describe('ConstanteMantenimientoComponent', () => {
  let component: ConstanteMantenimientoComponent;
  let fixture: ComponentFixture<ConstanteMantenimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConstanteMantenimientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstanteMantenimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
