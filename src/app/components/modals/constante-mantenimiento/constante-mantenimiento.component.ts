import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Constante } from 'src/app/models/constante';
import { ApisService } from 'src/app/services/apis.service';
import { CloseModalService } from 'src/app/services/close-modal.service';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { ValidateService } from 'src/app/services/validate.service';

@Component({
  selector: 'app-constante-mantenimiento',
  templateUrl: './constante-mantenimiento.component.html',
  styleUrls: ['./constante-mantenimiento.component.sass']
})
export class ConstanteMantenimientoComponent implements OnInit {
  public constante_regis:Constante = new Constante()
  constructor(
    public validaServ: ValidateService,
    private properties: PropertiesService,
    public apiServ: ApisService,
    private closeModal:CloseModalService,
    public dialogRef: MatDialogRef<ConstanteMantenimientoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {title: '', id: 0, superior: 0}
  ) {
   }

  ngOnInit(): void {
    console.log("data sueprio ===> " + this.data.superior)
    if (this.data.id > 0) this.obtener_tipoConstante()
    this.constante_regis.n_idsuperior = this.data.superior == 0 ? null : this.data.superior
    console.log("superior ==>"  + this.constante_regis.n_idsuperior)
  }

  obtener_tipoConstante() {
  this.apiServ.listado(`${this.apiServ.api_constante}get`, `?n_idconstante=${this.data.id}` ).then(response => {
      console.log(JSON.stringify(response));
      this.constante_regis = response;

    })
  }

  guardar() {
    console.log(JSON.stringify(this.constante_regis))
    let tipoapi = this.constante_regis.n_idconstante > 0 ? 'update' : 'insert'
    if (this.validaServ.validaConstante(this.constante_regis)) {
      if (this.constante_regis.n_idconstante > 0) {
        this.apiServ.actualizar(this.data.title, `${this.apiServ.api_constante}${tipoapi}`, this.constante_regis).then((response) => {
           if (response)  this.closeModal.onNoClickDialog(this.dialogRef, 1)
        })
      } else {
        this.apiServ.registro(this.data.title, `${this.apiServ.api_constante}${tipoapi}`, this.constante_regis).then((response) => {
           if (response)  this.closeModal.onNoClickDialog(this.dialogRef, 1)
        })
      }

    }
  }

  cancelar() {
    this.closeModal.cancelarModal(this.data.title, this.dialogRef)
  }

}
