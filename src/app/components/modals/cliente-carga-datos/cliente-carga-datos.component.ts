import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { APP_DATE_FORMATS, AppDateAdapter } from 'src/app/helpers/format-datepicker';
import { ClienteCarga } from 'src/app/models/cliente';
import { ApisService } from 'src/app/services/apis.service';
import { CloseModalService } from 'src/app/services/close-modal.service';
import { ComboService } from 'src/app/services/combo.service';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { DateService } from 'src/app/services/date.service';
import { PaginationModalService } from 'src/app/services/pagination-modal.service';
import { PaginationService } from 'src/app/services/pagination.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { ValidateService } from 'src/app/services/validate.service';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-cliente-carga-datos',
  templateUrl: './cliente-carga-datos.component.html',
  styleUrls: ['./cliente-carga-datos.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class ClienteCargaDatosComponent implements OnInit {

  public fecha_carga: Date = new Date()
  public hoy: number = -1;
  date: any;
  public importcliente: any;
  public errimport = false;
  public banconvert = false;
  public spin = false;
  public msjvalida = '';
  public validados = 0;
  dataGlobal:any=[]
  public totalregistros: number = 0;
  public filanumero: any = [];
  public filadia: any = [];
  public regnumero:string = '';
  public regfecha:string = '';
  public regmonto:string = '';
  public regplazo: string = '';
  public btndisreg: boolean = true
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;
  constructor(
    public validaServ: ValidateService,
    private properties: PropertiesService,
    public apiServ: ApisService,
    protected comboServ: ComboService,
    public dialogRef: MatDialogRef<ClienteCargaDatosComponent>,
    public formatter: NgbDateParserFormatter,
    private spinner: NgxSpinnerService,
    public pageServ: PaginationModalService,
    private closeModal: CloseModalService,
    public dateServ: DateService,
    @Inject(MAT_DIALOG_DATA) public data: {title: ''}
  ) {
    this.date = new FormControl(new Date());
    this.pageEvent = this.pageServ.inicializarModal()
    this.dataGlobal = [];
    this.pageServ.dataSourceModal = []
  }

  ngOnInit(): void {
  }

  guardar() {
    Swal.fire({
          title: this.data.title,
          text: "¿Seguro de guardar los números y generar la asignación?",
          icon: 'question',
          showCancelButton: true,
          confirmButtonText: 'Aceptar',
          cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        let request = {
          clientes: this.dataGlobal,
          fecha: this.dateServ.datepipe.transform(this.fecha_carga, "dd-MM-YYYY")
        }
        console.log(JSON.stringify(this.dataGlobal))
        this.apiServ.registro(this.data.title, `${this.apiServ.api_cliente}insert-masivo`, request).then((response) => {
           if (response)  this.closeModal.onNoClickDialog(this.dialogRef, 1)
        })
      }
    })
  }

  cancelar() {
    this.closeModal.cancelarModal(this.data.title, this.dialogRef)
  }

  dataFecha: any = [];
  async convert_clientes(e: any) {
    this.ordenar_dias()
    this.totalregistros = 0;
    this.dataGlobal = [];
    this.btndisreg = true
    this.pageServ.dataSourceModal = []
    this.redisetable();
    this.banconvert = true;
    this.msjvalida = 'Validando y convirtiendo datos...';
    this.errimport = false;
    let input = e.target as HTMLInputElement;
    let files:any = input.files;
    let file: File = files[0];
    let fileReader = new FileReader();
    fileReader.readAsArrayBuffer(file);
    let name = file.name.split('.');
    let extension = name[name.length - 1];

    fileReader.onload = () => {
      let arrayBuffer = fileReader.result;
      let data = new Uint8Array(arrayBuffer as ArrayBuffer);
      let arr = new Array();

      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      let bstr = arr.join('');
      let workbook = XLSX.read(bstr, { type: 'binary' });
      let first_sheet_name = workbook.SheetNames[0];

      let worksheet = workbook.Sheets[first_sheet_name];
      console.log(extension)
      if (extension === 'csv') {
        let datos: any = XLSX.utils.sheet_to_json(worksheet);
        console.log(JSON.stringify(datos))
        datos.forEach((element: { [x: string]: any; }, i: number) => {
          console.log(element["Phone 1 - Value"])
          setTimeout(()  => {
            this.validados += 1;
            this.msjvalida = `Datos de clientes validados con éxito :: ${this.validados} registros.`
            let fechaobt = this.dateServ.datepipe.transform(this.fecha_carga, "dd-MM-YYYY");
            this.dataGlobal.push({pos:i+1,numero: element["Phone 1 - Value"], fechawtsp: fechaobt, fecha: fechaobt})
            if (this.validados == datos.length) {
              this.spin = false
              this.btndisreg = false
            }
            this.redisetable();
          }, 3000);
        });

      } else {
        this.importcliente = null;
        this.errimport = true;
        this.msjvalida = 'El archivo que ha intentado cargar no es de formato excel.';
      }
      // if (extension === 'xls' || extension === 'xlsx') {
      //   let datos:any = XLSX.utils.sheet_to_json(worksheet);
      //   this.validados = 0;

      //   for (let i = 0; i < datos.length; i++) {
      //     setTimeout(()  => {
      //       this.validados += 1;
      //       this.msjvalida = `Datos de clientes validados con éxito :: ${this.validados} registros.`
      //       let fechaobt = this.convertir_fecha(datos[i]['fecha'].toString());
      //       this.dataGlobal.push({pos:i+1,numero: datos[i]['numero'].toString(), fechawtsp: datos[i]['fecha'], fecha: fechaobt})
      //       if (this.validados == datos.length) this.spin = false
      //       this.pageEvent = this.pageServ.inicializar()
      //       this.pageServ.dataSource = this.dataGlobal
      //       this.pageServ.coleccionsize = this.pageServ.dataSource.length
      //       this.collectionSize = this.pageServ.dataSource.length
      //       this.pageEvent.length = this.dataGlobal.length
      //       this.pageServ.actualizaTablaPage(this.dataGlobal, this.pageEvent);
      //     }, 3000);

      //   }
      // } else {
      //   this.importcliente = null;
      //   this.errimport = true;
      //   this.msjvalida = 'El archivo que ha intentado cargar no es de formato excel.';
      // }

    };

    console.log(JSON.stringify(this.dataGlobal))
    console.log(JSON.stringify(this.regfecha))
    console.log(JSON.stringify(this.regnumero))
  }

  redisetable() {
    this.pageEvent = this.pageServ.inicializarModal()
    this.pageServ.dataSourceModal = this.dataGlobal
    this.pageServ.coleccionsize = this.pageServ.dataSourceModal.length
    this.collectionSize = this.pageServ.dataSourceModal.length
    this.pageEvent.length = this.dataGlobal.length
    this.pageServ.actualizaTablaModal(this.dataGlobal, this.pageEvent);
  }

  limpiar_adj(): void {
    this.importcliente = null;
    this.errimport = false;
    this.banconvert = false;
  }

  ordenar_dias() {
    this.hoy = new Date(this.fecha_carga).getDay()
    console.log(this.hoy)
    let dif = 0;
    if (this.hoy == 1) {
      this.dataFecha.push({ nombre: this.dateServ.dias[this.hoy-1], dif: dif++ })
    } else {
      for (let j = this.hoy - 1; j >=0; j--){
        this.dataFecha.push({ nombre: this.dateServ.dias[j], dif: dif++ })
      }
    }
    for (let j = this.dateServ.dias.length - 1; j >= this.hoy; j--){
      this.dataFecha.push({ nombre: this.dateServ.dias[j], dif: dif++ })
    }
  }

  convertir_fecha(fechapar:string) {
    let fechaobt: string = fechapar;
    let fecha:any = "";
    if (fechaobt.includes(":")) {
      fecha = this.dateServ.datepipe.transform(new Date(this.fecha_carga), "dd-MM-YYYY")
    } else {
      if (fechaobt == 'Ayer') {
        fecha = this.dateServ.sumarDias(new Date(this.fecha_carga),  -1)
      } else {
        let pos = this.dataFecha.findIndex((obj: { nombre: string; dif: number }) => obj.nombre == fechaobt)
        if (pos > -1) {
          console.log(this.dataFecha[pos].dif)
          fecha = this.dateServ.sumarDias(new Date(this.fecha_carga), this.dataFecha[pos].dif * -1)
        } else {
          let fechaobtw: any = fechaobt.split("/")
          let obtfecha: Date = new Date(`${fechaobtw[2]}-${fechaobtw[1]}-${fechaobtw[0]}`)
          // console.log(this.segServ.browser)
          // obtfecha.setDate(obtfecha.getDate() + (this.segServ.browser.indexOf('Mozilla') > -1 ? 1 :  0))
          fecha = this.dateServ.datepipe.transform(obtfecha, "dd-MM-YYYY")
        }
      }
    }
    return fecha;
  }

  async exportclientes() {

  }

  async cargar_numeros(e: any) {
    let fecha = this.dateServ.datepipe.transform(new Date(this.fecha_carga), "dd-MM-YYYY")
    Swal.fire({
      title: this.data.title,
      text: `¿Está seguro de cargar números con fecha del día ${fecha}?`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      this.spin = true
      if (result.value) {
        this.convert_clientes(e)
      } else {
        this.spin = false
      }
    })

  }

}
