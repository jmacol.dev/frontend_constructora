import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteCargaDatosComponent } from './cliente-carga-datos.component';

describe('ClienteCargaDatosComponent', () => {
  let component: ClienteCargaDatosComponent;
  let fixture: ComponentFixture<ClienteCargaDatosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClienteCargaDatosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteCargaDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
