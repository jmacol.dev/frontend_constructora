import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ValidateService } from 'src/app/services/validate.service';
import Swal from 'sweetalert2';
import axios from "axios";
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-olvide-clave',
  templateUrl: './olvide-clave.component.html',
  styleUrls: ['./olvide-clave.component.sass']
})
export class OlvideClaveComponent implements OnInit {

  public title: string;
  public rute: string;
  public usuario_usu: string = "";
  public email_usu: string = "";
  public _load: boolean = false;
  constructor(
    protected valserv: ValidateService,
    private spinner: NgxSpinnerService,
    public configServ: ConfiguracionService,
    public segServ: SeguridadService
  ) {
    this.title = "Olvide mi contraseña";
    this.rute = "/olvideclave";
  }

  ngOnInit(): void {
    this.cancelar()
  }

  async recuperar(captchaResponse: string) {
    this._load = true;
      let data = {
        usuario: this.usuario_usu,
        email: this.email_usu,
        tokenrecaptcha: captchaResponse,
      };
      if (this.valserv.validaterecuperar(this.usuario_usu, this.email_usu, this.title)) {
        console.log("TOKEN LOGIN: " + JSON.stringify(data));
        await axios
          .post(environment.URLAPI + "auth/olvide-password", data)
          .then((response) => {
            console.log(response.data);
            let datos = response.data;
            if (datos.estado) {
              this.spinner.hide();
              this._load = false;
              Swal.fire(this.title, datos.mensaje, "success").then(
                () => {
                  this.cancelar();
                  location.href = environment.INDEX + "login";
                }
              );
            } else {
              this.spinner.hide();
              this._load = false;
              Swal.fire("Recuperar contraseña", datos.mensaje, "info");
            }
          })
          .catch( (error) =>{
            this._load = false;
            this.spinner.hide();
            Swal.fire("Ha ocurrido un error al registrar!", error.message, "error");
            console.log(error);
          });
      } else {
        this.spinner.hide();
        this._load = false;
      }

  }

  cancelar() {
    this.usuario_usu = "";
    this.email_usu = "";
  }

}
