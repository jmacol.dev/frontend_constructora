import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { ValidateService } from 'src/app/services/validate.service';
import Swal from 'sweetalert2';
import axios from "axios";
import { environment } from 'src/environments/environment';
import { Usuario } from 'src/app/models/usuario';
import { SesionService } from 'src/app/services/sesion.service';
import { CloseModalService } from 'src/app/services/close-modal.service';
@Component({
  selector: 'app-cambiarpass',
  templateUrl: './cambiarpass.component.html',
  styleUrls: ['./cambiarpass.component.sass']
})
export class CambiarpassComponent implements OnInit {
  hide = true;
  hiden = true;
  hidec = true;

  public title: string;
  public rute: string;
  public pass_act: string = "";
  public pass_usu: string = "";
  public passc_usu: string = "";
  public id_usu: number = 0

  constructor(
    public configserv: ConfiguracionService,
    public dialogRef: MatDialogRef<CambiarpassComponent>,
     protected valserv: ValidateService,
    private spinner: NgxSpinnerService,
    public segServ: SeguridadService,
    public sserv: SesionService,
    public closeModal:CloseModalService,
    @Inject(MAT_DIALOG_DATA) public data: { title: '' }) {
    this.title = "Cambiar contraseña";
    this.rute = "/cambiar-clave";
     }

  ngOnInit(): void {

  }

  async actualizar_clave() { //captchaResponse: string
      let _ban = true;
    let _mensaje = "";
    let usu: Usuario = JSON.parse(this.sserv.getuser(localStorage.getItem('user')??'{}') ?? '{}')
    let data = {
        actualpass: this.pass_act,
        nuevapass: this.pass_usu,
        confimapass: this.passc_usu,
        tokenrecaptcha: "",
        id: usu.n_idusuario
      };


    if (this.valserv.validatecambiarclave(this.pass_act, this.pass_usu, this.passc_usu, this.title)) {
       this.spinner.show();
        console.log("TOKEN LOGIN: " + JSON.stringify(data));
        await axios
          .post(environment.URLAPI + "auth/cambiar-clave", data)
          .then((response) => {
            console.log(response.data);
            let datos = response.data;
            if (datos.estado) {
              this.spinner.hide();
              Swal.fire(this.title, datos.mensaje, "success").then(
                (response) => {
                  this.cancel();
                  this.closeModal.onNoClickDialog(this.dialogRef)
                  this.segServ.limpiarsesion()
                }
              );
            } else {
              this.spinner.hide();
              Swal.fire(this.title, datos.mensaje, "info");
            }
          })
          .catch( (error) =>{
            this.spinner.hide();
            Swal.fire("Ha ocurrido un error al registrar!", error.message, "error");
            console.log(error);
          });
      } else {
        this.spinner.hide();
      }
  }
  cancel() {
    this.pass_usu = "";
    this.passc_usu = "";
  }

}
