import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params  } from '@angular/router';
import { ValidateService } from 'src/app/services/validate.service';
import Swal from 'sweetalert2';
import axios from "axios";
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-recuperar-clave',
  templateUrl: './recuperar-clave.component.html',
  styleUrls: ['./recuperar-clave.component.sass']
})
export class RecuperarClaveComponent implements OnInit {
  recaptchaAvailable = false;
  public title: string;
  public rute: string;
  public pass_usu: string = "";
  public passc_usu: string = "";
  public id_usu: number =  0
  public codigocambio: string =  ""
  public _load: boolean = false;
  constructor(
    private actroute: ActivatedRoute,
     protected valserv: ValidateService,
    private spinner: NgxSpinnerService,
    public configServ: ConfiguracionService,
    public segServ: SeguridadService
  ) {
     this.title = "Nueva contraseña";
    this.rute = "/nueva-clave";
   }

  ngOnInit(): void {

    console.log("login.component cargado!");
    this.cancel()
    this.actroute.queryParams
      .subscribe(params => {
        console.log(params.codigoconfirmacion)
        console.log(atob(
          params.codigoconfirmacion
        ))
        this.codigocambio = atob(
          params.codigoconfirmacion
        );
      }
    );
    this.actroute.queryParams
      .subscribe(params => {
        this.id_usu = parseInt(atob(params.id));
      }
    );
  }

  async actualizar_clave(captchaResponse: string) {
    this._load = true;

      // this.spinner.show();

      let _ban = true;
      let _mensaje = "";
      let data = {
        nuevapass: this.pass_usu,
        confimapass: this.passc_usu,
        tokenrecaptcha: captchaResponse,
        id: this.id_usu,
        codigocambio: this.codigocambio
      };
      // this.spinner.show();

     if (this.valserv.validatenuevaclave(this.pass_usu, this.passc_usu, this.codigocambio,this.title)) {
        console.log("TOKEN LOGIN: " + JSON.stringify(data));
        await axios
          .post(environment.URLAPI + "auth/nueva-clave", data)
          .then((response) => {
            console.log(response.data);
            let datos = response.data;
            if (datos.estado) {
              this.spinner.hide();
              this._load = false;
              Swal.fire(this.title, datos.mensaje, "success").then(
                (response) => {
                  this.cancel();
                  location.href = environment.INDEX + "login";
                }
              );
            } else {
              this.spinner.hide();
              this._load = false;
              Swal.fire(this.title, datos.mensaje, "info");
            }
          })
          .catch( (error) =>{
            this._load = false;
            this.spinner.hide();
            Swal.fire("Ha ocurrido un error al registrar!", error.message, "error");
            console.log(error);
          });
      } else {
        this.spinner.hide();
        this._load = false;
      }
  }
  cancel() {
    this.pass_usu = "";
    this.passc_usu = "";
    this.codigocambio= "";
  }

}
