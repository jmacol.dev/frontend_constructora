import { FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Trabajador } from 'src/app/models/trabajador';
import { ApisService } from 'src/app/services/apis.service';
import { OutEventService } from 'src/app/services/out-event.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { PaginationService } from 'src/app/services/pagination.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-trabajador',
  templateUrl: './trabajador.component.html',
  styleUrls: ['./trabajador.component.sass']
})
export class TrabajadorComponent implements OnInit {


  public dataGlobal: Trabajador[] = []
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;
  search:string = ""
  public banperm: boolean = false
  myControl = new FormControl();
  title: string = "";
  route: string = ""
  modulo: string = '';
  @ViewChild('drawer') sideNav: any;
  constructor(
    public outServ: OutEventService,
    public apiServ: ApisService,
    public properties: PropertiesService,
    public pageServ: PaginationService,
    public sidenavServ: SidenavService,
    public configServ: ConfiguracionService,
    private spinner: NgxSpinnerService,
  ) {
    this.title = properties.trabajador.titulo;
    this.route = properties.datos_route.trabajador.route;
    this.modulo = properties.datos_route.trabajador.modulo;
  }

  ngOnInit(): void {
    this.pageEvent = this.pageServ.inicializar()
    this.lista_trabajadores()
  }

  ngAfterViewInit(): any {
    this.sidenavServ.abrirmenu(this.sideNav);
    this.outServ.nuevo = () => {
      this.configServ.modalServ.trabajadorModal(0);
    };
    this.outServ.listar = () => {
      console.log('Listado de Trabajador');
      this.lista_trabajadores()
    }
    this.outServ.excel = () => {
      console.log("exportar")
    }

    this.outServ.pdf = () => {
      console.log("exportar pdf")
    }
  }

  applyFilter() {
    this.search = this.search.toLowerCase(); // Datasource defaults to lowercase matches
    let data = this.dataGlobal.filter(item => (item.v_nombres).toLowerCase().includes(this.search) || (item.v_apellidos).toLowerCase().includes(this.search)|| (item.v_numdoc).toLowerCase().includes(this.search) );
    this.rediseTablaPage(data);
  }


  lista_trabajadores() {
    this.spinner.show();
    this.apiServ.listado(`${this.apiServ.api_trabajador}list`, '' ).then(response => {
      console.log(JSON.stringify(response));
      this.pageEvent = this.pageServ.inicializar()
        console.log("pasa")
        console.log(JSON.stringify(response))
        this.dataGlobal = response
        this.rediseTablaPage(response);
    }).finally(() => {
      this.spinner.hide();
    })
  }

  rediseTablaPage(data: any) {
      this.pageServ.coleccionsize = this.pageServ.dataSource.length
      this.pageServ.dataSource = data
      this.collectionSize = this.pageServ.dataSource.length
      this.pageEvent.length = data.length
      this.pageServ.actualizaTablaPage(data, this.pageEvent);
  }

  estado(id: number, estado: any) {
    let $request = {
      n_idtrabajador: id,
      b_vigencia: estado
    }
    console.log(estado)

    Swal.fire({
          title: this.title,
          text: estado == 0 ? this.properties.confirminhab : this.properties.confirmactiv,
          icon: 'question',
          showCancelButton: true,
          confirmButtonText: 'Aceptar',
          cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.apiServ.estado(this.title, `${this.apiServ.api_trabajador}delete`, $request).then(() => {
          this.outServ.listar(null)
        })
      }
    })

  }

  editar(id: number) {
    this.configServ.modalServ.trabajadorModal(id);
  }

}
