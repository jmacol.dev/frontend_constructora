import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ApisService } from 'src/app/services/apis.service';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { OutEventService } from 'src/app/services/out-event.service';
import { PaginationService } from 'src/app/services/pagination.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import { FormControl } from '@angular/forms';
import { Cliente } from 'src/app/models/cliente';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
//import { spawn } from 'child_process';
//const {spawn} = require('child_process');


@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.sass']
})
export class ClienteComponent implements OnInit {
  public dataGlobal: Cliente[] = []
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;
  search:string = ""
  public banperm: boolean = false
  myControl = new FormControl();
  title: string = "";
  route: string = ""
  modulo: string = '';
  @ViewChild('drawer') sideNav: any;
  constructor(
    public outServ: OutEventService,
    public apiServ: ApisService,
    public properties: PropertiesService,
    public pageServ: PaginationService,
    public sidenavServ: SidenavService,
    public configServ: ConfiguracionService,
    private spinner: NgxSpinnerService,
  ) {
    this.title = properties.cliente.titulo;
    this.route = properties.datos_route.cliente.route;
    this.modulo = properties.datos_route.cliente.modulo;
  }

  ngOnInit(): void {
    this.pageEvent = this.pageServ.inicializar()
    this.lista_clientes()
  }

   ngAfterViewInit(): any {
    this.sidenavServ.abrirmenu(this.sideNav);
    this.outServ.nuevo = () => {
      this.configServ.modalServ.clienteModal(0);
      //console.log("nuevo")
     };

     this.outServ.carga = () => {
     console.log("carga")
     this.configServ.modalServ.clienteCargaModal()
    };
    this.outServ.listar = () => {
      console.log('Listado de Clientes');
      this.lista_clientes()
    }
    this.outServ.excel = () => {
      console.log("exportar")
    }

    this.outServ.pdf = () => {
      console.log("exportar pdf")
    }
  }

  applyFilter() {
    this.search = this.search.toLowerCase();
    let data = this.dataGlobal.filter(item => (item.v_dni).toLowerCase().includes(this.search) || (item.v_numero).toLowerCase().includes(this.search)|| (item.v_nombre).toLowerCase().includes(this.search) );
    this.rediseTablaPage(data);
  }

  lista_clientes() {
    this.pageEvent = this.pageServ.inicializar()
    this.spinner.show();
    this.apiServ.listado(`${this.apiServ.api_cliente}list`, '' ).then(response => {
      console.log(JSON.stringify(response));
        this.dataGlobal = response
      this.rediseTablaPage(response);

    }).finally(() => {
      this.spinner.hide();
    })
  }
  rediseTablaPage(data: any) {
    this.pageServ.coleccionsize = this.pageServ.dataSource.length
    this.pageServ.dataSource = data
    this.collectionSize = this.pageServ.dataSource.length
    this.pageEvent.length = data.length
    this.pageServ.actualizaTablaPage(data, this.pageEvent);
  }

  estado(id: number, estado: any) {
    let $request = {
      n_idcliente: id,
      b_vigencia: estado
    }
    Swal.fire({
          title: this.title,
          text: estado == 0 ? this.properties.confirminhab : this.properties.confirmactiv,
          icon: 'question',
          showCancelButton: true,
          confirmButtonText: 'Aceptar',
          cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.apiServ.estado(this.title, `${this.apiServ.api_cliente}delete`, $request).then(() => {
          this.outServ.listar(null)
        })
      }
    })
  }

  editar(id: number) {
    this.configServ.modalServ.clienteModal(id);
  }

  resultados($obj: Cliente) {
    this.configServ.modalServ.listaResultadosModal($obj.n_idcliente, $obj)
  }

}
