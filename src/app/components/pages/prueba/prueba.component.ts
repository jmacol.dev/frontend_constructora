import { FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Prueba } from 'src/app/models/prueba';
import { ApisService } from 'src/app/services/apis.service';
import { OutEventService } from 'src/app/services/out-event.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { PaginationService } from 'src/app/services/pagination.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import * as XLSX from 'xlsx';
import { Usuario } from 'src/app/models/usuario';
import { Trabajador } from 'src/app/models/trabajador';
@Component({
  selector: 'app-prueba',
  templateUrl: './prueba.component.html',
  styleUrls: ['./prueba.component.sass']
})
export class PruebaComponent implements OnInit {

  public dataGlobal: Prueba[] = []
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;
  search:string = ""
  public banperm: boolean = false
  myControl = new FormControl();
  title: string = "";
  route: string = ""
  modulo: string = '';

  @ViewChild('drawer') sideNav: any;

  constructor(
    public outServ: OutEventService,
    public apiServ: ApisService,
    public properties: PropertiesService,
    public pageServ: PaginationService,
    public sidenavServ: SidenavService,
    public configServ: ConfiguracionService,
    private spinner: NgxSpinnerService,
  ) {
    this.title = properties.prueba.titulo;
    this.route = properties.datos_route.prueba.route;
    this.modulo = properties.datos_route.prueba.modulo;
  }

  ngOnInit(): void {
    this.lista_pruebas()
  }

  ngAfterViewInit(): any {
    this.sidenavServ.abrirmenu(this.sideNav);
    this.outServ.nuevo = () => {
      this.configServ.modalServ.pruebaModal(0);
    };
    this.outServ.listar = () => {
      console.log('lista pruba');
      this.dataGlobal = []
      this.lista_pruebas()
    }
    this.outServ.excel = () => {
      console.log("exportar")
    }

    this.outServ.pdf = () => {
      console.log("exportar pdf")
    }
  }

  applyFilter() {
    this.search = this.search.toLowerCase(); // Datasource defaults to lowercase matches
    let data = this.dataGlobal.filter(item => (item.v_cadena).toLowerCase().includes(this.search));
    this.rediseTablaPage(data);
  }


  lista_pruebas() {
    this.spinner.show();
    this.apiServ.listado(`${this.apiServ.api_prueba}list`, '' ).then(response => {
      console.log(JSON.stringify(response));
      this.pageEvent = this.pageServ.inicializar()
      if (response.length == 0) {
        this. pageServ.dataSource = []
      }
      if (response.length != this.dataGlobal.length) {
        console.log("pasa")
        this.dataGlobal = response
        this.rediseTablaPage(response);
      }
    }).finally(() => {
      this.spinner.hide();
    })
  }

  rediseTablaPage(data: any) {
      this.pageServ.coleccionsize = this.pageServ.dataSource.length
      this.pageServ.dataSource = data
      this.collectionSize = this.pageServ.dataSource.length
      this.pageEvent.length = data.length
      this.pageServ.actualizaTablaPage(data, this.pageEvent);
  }

  estado(id: number, estado: any) {
    let $request = {
      n_id: id,
      b_vigencia: estado
    }
    console.log(estado)

    Swal.fire({
          title: this.title,
          text: estado == 0 ? this.properties.confirminhab : this.properties.confirmactiv,
          icon: 'question',
          showCancelButton: true,
          confirmButtonText: 'Aceptar',
          cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.apiServ.estado(this.title, `${this.apiServ.api_prueba}delete`, $request).then(() => {
          this.outServ.listar(null)
        })
      }
    })

  }

  editar(id: number) {
    let trab: Trabajador = new Trabajador()
    let user = null;
    if (id > 0) {
      user = {
        v_usuario: 'jmacol',
        a_v_ipcrea: '0.0.0.0',
        b_vigencia: 1
      }
    }
    let request = {
      n_idtrabajador: trab.n_idtrabajador,
      v_numdoc: trab.v_numdoc,
      v_nombres: trab.v_nombres,
      v_apellidos: trab.v_apellidos,
      v_telefono: trab.v_telefono,
      v_email: trab.v_email,
      n_idtipodoc: trab.n_idtipodoc,
      usuario:user
    }

    this.configServ.modalServ.pruebaModal(id);
  }
}
