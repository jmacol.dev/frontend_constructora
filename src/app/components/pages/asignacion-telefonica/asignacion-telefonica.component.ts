import { FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import {AsignacionTelefonica, TrabajadorCombo } from 'src/app/models/asignacion-telefonica';
import { ApisService } from 'src/app/services/apis.service';
import { OutEventService } from 'src/app/services/out-event.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { PaginationService } from 'src/app/services/pagination.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import * as XLSX from 'xlsx';
import { Usuario } from 'src/app/models/usuario';
import { Trabajador } from 'src/app/models/trabajador';
import { PageEvent } from '@angular/material/paginator';
import { ComboService } from 'src/app/services/combo.service';
import { APP_DATE_FORMATS, AppDateAdapter } from 'src/app/helpers/format-datepicker';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { DateService } from 'src/app/services/date.service';
import { DetalleAsignacion } from 'src/app/models/detalle-asignacion';
import { Cliente } from 'src/app/models/cliente';
import { SesionService } from 'src/app/services/sesion.service';

@Component({
  selector: 'app-asignacion-telefonica',
  templateUrl: './asignacion-telefonica.component.html',
  styleUrls: ['./asignacion-telefonica.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class AsignacionTelefonicaComponent implements OnInit {

  public dataGlobal: AsignacionTelefonica[] = []
  public dataGlobalD: DetalleAsignacion[] = []
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;
  search:string = ""
  public bandetalle: boolean = false
  myControl = new FormControl();
  title: string = "";
  route: string = ""
  modulo: string = '';
  objsel!: AsignacionTelefonica;
  idasignacion: number = 0;
  @ViewChild('drawer') sideNav: any;
  comboTrabajador: TrabajadorCombo[] = [];
  public idtrabajador: number = 0;
  public fechainicio !: Date;
  public fechafin !: Date;
  rangeDate: FormGroup = new FormGroup({});
  public showItem: boolean = true
  usu: Usuario = new Usuario();
  constructor(
    public outServ: OutEventService,
    public apiServ: ApisService,
    public properties: PropertiesService,
    public pageServ: PaginationService,
    public sidenavServ: SidenavService,
    public configServ: ConfiguracionService,
    public dateServ: DateService,
    private spinner: NgxSpinnerService,
    protected sserv: SesionService,
    protected comboServ: ComboService,
  ) {
    this.title = properties.asignaciontelefonica.titulo;
    this.route = properties.datos_route.asignaciontelefonica.route;
    this.modulo = properties.datos_route.asignaciontelefonica.modulo;
    this.comboServ.combo_trabajador(this.properties.tipoconstante.tipodocumento).then(dataCombo => {
      this.comboTrabajador = dataCombo;
      this.selectOnChange("0")
    })
    this.usu = JSON.parse(this.sserv.getuser(localStorage.getItem('user')??'{}') ?? '{}')
    this.showItem = (this.properties.id_admin == this.usu.n_idusuario)

  }

  ngOnInit(): void {
    this.pageServ.inicializar();
    this.lista_asignaciontelefonica()
    this.rangeDate = new FormGroup({
        start: new FormControl(this.dateServ.fechamin),
        end: new FormControl(this.dateServ.fechamax)
      });
  }

  ngAfterViewInit(): any {
    this.sidenavServ.abrirmenu(this.sideNav);
    this.outServ.listar = () => {
      console.log('lista Asignacion Telefonica');
      this.pageEvent = this.pageServ.inicializar()
      if (this.bandetalle) {
        this.dataGlobalD = [];
        this.lista_detalleasignacion(this.idasignacion)
      } else {
        this.dataGlobal = []
        this.lista_asignaciontelefonica()
      }

    }
    this.outServ.regresar = () => {
       this.regresar()
    }
  }

  selectOnChange(idtrabajador: any) {
      this.idtrabajador = idtrabajador
  }

  applyFilter() {
    this.search = this.search.toLowerCase(); // Datasource defaults to lowercase matches
    let data = this.dataGlobal.filter(item => (item.trabajador.v_nombres).toLowerCase().includes(this.search)||(item.trabajador.v_apellidos).toLowerCase().includes(this.search));
    this.rediseTablaPage(data);
  }


  lista_asignaciontelefonica() {
    this.pageEvent = this.pageServ.inicializar()
    this.spinner.show();
    console.log("usuario reg ===> " + JSON.stringify(this.usu))
    if (!this.showItem) this.idtrabajador = this.usu.trabajador.n_idtrabajador
    let request = `?fechainicio=${this.dateServ.datepipe.transform(this.dateServ.fechamin, "dd-MM-YYYY")}&fechafin=${this.dateServ.datepipe.transform(this.dateServ.fechamax, "dd-MM-YYYY")}&idtrabajador=${this.idtrabajador}`
    console.log(request)
    this.apiServ.listado(`${this.apiServ.api_asignatelefono}list`, request ).then(response => {
      console.log(JSON.stringify(response));
      this.pageEvent = this.pageServ.inicializar()
      this.pageServ.dataSource = []
      this.dataGlobal = response
      this.rediseTablaPage(response);
    }).finally(() => {
      this.spinner.hide();
    })
  }

  rediseTablaPage(data: any) {
      this.pageServ.coleccionsize = this.pageServ.dataSource.length
      this.pageServ.dataSource = data
      this.collectionSize = this.pageServ.dataSource.length
      this.pageEvent.length = data.length
      this.pageServ.actualizaTablaPage(data, this.pageEvent);
  }

  estado(id: number, estado: any) {
    let $request = {
      n_id: id,
      b_vigencia: estado
    }
    console.log(estado)

    Swal.fire({
          title: this.title,
          text: estado == 0 ? this.properties.confirminhab : this.properties.confirmactiv,
          icon: 'question',
          showCancelButton: true,
          confirmButtonText: 'Aceptar',
          cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.apiServ.estado(this.title, `${this.apiServ.api_asignatelefono}delete`, $request).then(() => {
          this.outServ.listar(null)
        })
      }
    })

  }

  editar(id: number) {
    let trab: Trabajador = new Trabajador()
    let user = null;
    if (id > 0) {
      user = {
        v_usuario: 'jmacol',
        a_v_ipcrea: '0.0.0.0',
        b_vigencia: 1
      }
    }
    let request = {
      n_idtrabajador: trab.n_idtrabajador,
      v_numdoc: trab.v_numdoc,
      v_nombres: trab.v_nombres,
      v_apellidos: trab.v_apellidos,
      v_telefono: trab.v_telefono,
      v_email: trab.v_email,
      n_idtipodoc: trab.n_idtipodoc,
      usuario:user
    }


  }

  detalle($obj: AsignacionTelefonica) {
    this.bandetalle = true
    this.objsel = $obj;
    this.idasignacion = $obj.n_idasignacion
    // this.dataGlobalD = []
    // this.pageEvent = this.pageServ.inicializar()
    // this.rediseTablaPage(this.dataGlobalD);
    this.outServ.listar(null)
  }

  lista_detalleasignacion($idasigna: number) {
    this.pageEvent = this.pageServ.inicializar()
    this.spinner.show();
    let request = `?n_idasignacion=${$idasigna}`
    console.log(request)
    this.apiServ.listado(`${this.apiServ.api_detalleasigna}list`, request ).then(response => {
      console.log(JSON.stringify(response));
      this.pageEvent = this.pageServ.inicializar()
      this.pageServ.dataSource = []
      this.dataGlobalD = response
      this.rediseTablaPage(response);
    }).finally(() => {
      this.spinner.hide();
    })
  }


  regresar() {
    this.bandetalle = false
    this.idasignacion = 0
    this.outServ.listar(null)
  }

  registro_resultado($obj: DetalleAsignacion) {
    this.configServ.modalServ.resultadoAsignacionModal($obj.n_idasignadeta, $obj)
  }

  editar_cliente($idcliente: number) {
    this.configServ.modalServ.clienteModal($idcliente);
  }

  resultados($obj: Cliente) {
    this.configServ.modalServ.listaResultadosModal($obj.n_idcliente, $obj)
  }

}
