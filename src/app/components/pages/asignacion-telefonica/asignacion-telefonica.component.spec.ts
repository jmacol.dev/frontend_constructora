import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionTelefonicaComponent } from './asignacion-telefonica.component';

describe('AsignacionTelefonicaComponent', () => {
  let component: AsignacionTelefonicaComponent;
  let fixture: ComponentFixture<AsignacionTelefonicaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsignacionTelefonicaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionTelefonicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
