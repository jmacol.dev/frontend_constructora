import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { NgxSpinnerService } from 'ngx-spinner';
import { Constante } from 'src/app/models/constante';
import { ApisService } from 'src/app/services/apis.service';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { OutEventService } from 'src/app/services/out-event.service';
import { PaginationService } from 'src/app/services/pagination.service';
import { PropertiesService } from 'src/app/services/properties.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-constante',
  templateUrl: './constante.component.html',
  styleUrls: ['./constante.component.sass']
})
export class ConstanteComponent implements OnInit {
  title: string = "";
  route: string = ""
  modulo: string = '';
  public dataGlobal: Constante[] = []
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;
  search:string = ""
  public banperm: boolean = false
  myControl = new FormControl();
  @ViewChild('drawer') sideNav: any;
  constructor(
    public outServ: OutEventService,
    public apiServ: ApisService,
    public properties: PropertiesService,
    public pageServ: PaginationService,
    public sidenavServ: SidenavService,
    public configServ: ConfiguracionService,
    public spinner: NgxSpinnerService
  ) {
    this.title = properties.constante.titulo;
    this.route = properties.datos_route.constante.route;
    this.modulo = properties.datos_route.constante.modulo;
  }

  ngOnInit(): void {
    this.lista_constantes()
  }

   ngAfterViewInit(): any {
    this.sidenavServ.abrirmenu(this.sideNav);
    this.outServ.nuevo = () => {
      console.log("nuevo")
      this.configServ.modalServ.constanteModal(0,0)
     };

     this.outServ.listar = () =>  {
      this.lista_constantes()
      console.log("listar ==> " )
    }
   }

  applyFilter() {
    this.search = this.search.toLowerCase(); // Datasource defaults to lowercase matches
    let data = this.dataGlobal.filter(item => (item.v_nombre).toLowerCase().includes(this.search) || (item.v_descripcion).toLowerCase().includes(this.search));
    this.rediseTablaPage(data);
  }

  lista_constantes() {
    this.spinner.show();
    this.apiServ.listado(`${this.apiServ.api_constante}list`, `?n_idsuperior=` ).then(response => {
      console.log(JSON.stringify(response));
      this.pageEvent = this.pageServ.inicializar()
      if (response.length == 0) {
        this. pageServ.dataSource = []
      }else {
        console.log("pasa")
        this.dataGlobal = response
        this.rediseTablaPage(response);
      }
    }).finally(() => {
      this.spinner.hide();
    })
  }

  rediseTablaPage(data: any) {
      this.pageServ.coleccionsize = this.pageServ.dataSource.length
      this.pageServ.dataSource = data
      this.collectionSize = this.pageServ.dataSource.length
      this.pageEvent.length = data.length
      this.pageServ.actualizaTablaPage(data, this.pageEvent);
  }

  editar($id: number) {
    this.configServ.modalServ.constanteModal($id, 0);
  }

  detalle($id: number) {
    this.configServ.modalServ.constanteDetalleModal($id);
  }

  estado(id: number, estado: any) {

    let $request = {
      n_idconstante: id,
      b_vigencia: estado
    }
    console.log(estado)

    Swal.fire({
          title: this.title,
          text: estado == 0 ? this.properties.confirminhab : this.properties.confirmactiv,
          icon: 'question',
          showCancelButton: true,
          confirmButtonText: 'Aceptar',
          cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.apiServ.estado(this.title, `${this.apiServ.api_constante}delete`, $request).then(() => {
          this.outServ.listar(1)
        })
      }
    })
  }

}
