import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/home/dashboard/dashboard.component';
import { LoginComponent } from './components/auth/login/login.component';
import { ButtonComponent } from './components/template/button/button.component';
import { PruebaComponent } from './components/pages/prueba/prueba.component';
import { TrabajadorComponent } from './components/pages/trabajador/trabajador.component';
import { ActivoGuard } from './guards/activo.guard';
import { OlvideClaveComponent } from './components/auth/olvide-clave/olvide-clave.component';
import { RecuperarClaveComponent } from './components/auth/recuperar-clave/recuperar-clave.component';
import { ClienteComponent } from './components/pages/cliente/cliente.component';
import { ConstanteComponent } from './components/pages/constante/constante.component';
import { InactivoGuard } from './guards/inactivo.guard';
import { AsignacionTelefonicaComponent } from './components/pages/asignacion-telefonica/asignacion-telefonica.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent , canActivate:[InactivoGuard]},
  { path: "olvide-clave", component: OlvideClaveComponent },
  { path: "recuperar-password", component: RecuperarClaveComponent },
  { path: 'prueba', component: PruebaComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard]  },
  { path: 'trabajador', component: TrabajadorComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard]  },
  { path: 'cliente', component: ClienteComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard]  },
  { path: 'dashboard', component: DashboardComponent, runGuardsAndResolvers: 'always', canActivate: [ActivoGuard] },
  { path: 'constante', component: ConstanteComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard]  },
  { path: 'asignacion-telefonica', component: AsignacionTelefonicaComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard]  },
  
  { path: '', redirectTo: 'login', pathMatch:'full' },
  { path: '**', redirectTo: 'login'},

  //  canActivate:[ActivoGuard]
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
