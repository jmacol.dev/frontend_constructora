import { TestBed } from '@angular/core/testing';

import { InactivoGuard } from './inactivo.guard';

describe('InactivoGuard', () => {
  let guard: InactivoGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(InactivoGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
