import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginComponent } from '../components/auth/login/login.component';
import { DashboardComponent } from '../components/home/dashboard/dashboard.component';
import { SeguridadService } from '../services/seguridad.service';

@Injectable({
  providedIn: 'root'
})
export class ActivoGuard implements CanActivate{
  constructor(private router: Router,
  private segServ: SeguridadService) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      if (this.segServ.isLoggedIn()) {
        return true;
      }
        // ir a Login
      this.router.navigate(['/login']);
      return false;
  }
}
