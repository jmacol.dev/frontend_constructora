import { TestBed } from '@angular/core/testing';

import { PaginationModalService } from './pagination-modal.service';

describe('PaginationModalService', () => {
  let service: PaginationModalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaginationModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
