import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OutEventService {

  constructor() { }

  public listar(listar: any){
    return listar;
  }

  public listarModal(listarmodal: any){
    return listarmodal;
  }

  public excel(excel: any){
    return excel;
  }

  public pdf(pdf: any){
    return pdf;
  }

  public nuevo(nuevo: any){
    return nuevo;
  }

  public carga(carga: any){
    return carga;
  }

  public regresar(regresar: any){
    return regresar;
  }
}
