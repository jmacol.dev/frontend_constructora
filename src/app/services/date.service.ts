import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateService {
  public meses = ["ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SETIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"]
  public dias = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];
  public datepipe: DatePipe = new DatePipe('en-US')
  public today: Date | undefined;
  public datepast: Date | undefined;
  public fechamax!: Date;
  public fechamin!: Date;
  constructor() {
    this.today = new Date();
    this.datepast = new Date();
    this.datepast.setDate(this.datepast.getDate() - 2);

    this.fechamax = new Date();
    this.fechamin = new Date();
    this.fechamin.setDate(this.fechamin.getDate() - 7);
  }

  fecha_act($fecha: Date) {
    var fechaact = new Date($fecha)
    return new Date(fechaact.setDate(fechaact.getDate() + 1))
  }

  sumarDias(fecha:Date, dias:number){
    fecha.setDate(fecha.getDate() + dias);
    return this.datepipe.transform(fecha, "dd-MM-YYYY");
  }
}
