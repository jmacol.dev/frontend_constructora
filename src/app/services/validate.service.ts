import { PropertiesService } from 'src/app/services/properties.service';
import { ToastrService } from 'ngx-toastr';
import { SeguridadService } from './seguridad.service';
import { NotificacionesService } from './notificaciones.service';
import { Injectable } from '@angular/core';
import { Prueba } from '../models/prueba';
import { isNumber } from 'highcharts';
import { Trabajador } from '../models/trabajador';
import { Constante } from '../models/constante';
import { Cliente } from '../models/cliente';

@Injectable({
  providedIn: 'root'
})
export class ValidateService {
  public validaban: boolean = true;
  // public validatext: string = '';
  // public validatype: string = 'danger';
  public maxlength: number = 50
  public maxlengthcom: number = 200
  constructor(private toastr: ToastrService,
  private properties: PropertiesService) { }

  showError(msj: string, title: string) {
    this.toastr.error(msj, title);
  }

  showSuccess(msj: string, title: string) {
    this.toastr.success(msj, title);
  }

  showInfo(msj: string, title: string) {
    this.toastr.info(msj, title, {timeOut: 30000});
  }
  validatelogin($usuario: string, $password: string): boolean{
    this.validaban = true;
    if($usuario.trim() == '' && $password.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese sus credencales', "Inicio de Sesión")
    }else if($usuario.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese su usuario', "Inicio de Sesión")
    }else if($password.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese su contraseña', "Inicio de Sesión")
    }
    return this.validaban;
  }

  validaterecuperar($usuario: string, $email: string, $title: string): boolean{
    this.validaban = true;
    if($usuario.trim() == '' && $email.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese sus datos', $title)
    }else if($usuario.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese su usuario', $title)
    }else if($email.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese su email', $title)
    }
    return this.validaban;
  }

  validatenuevaclave(pass_usu: string, passc_usu: string, codigocambio: string, $title: string) {
    let _mensaje = ""
    this.validaban = true;
    const regex : RegExp = /^((?!.*[\s])(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{8,99})/;
    if (pass_usu.trim() == "" && passc_usu.trim() == "") {
        this.validaban = false;
        _mensaje = "Ingrese su contraseña nueva.";
      } else if (pass_usu.trim() == "") {
        this.validaban = false;
        _mensaje = "Ingrese su contraseña.";
      } else if (pass_usu.trim().length < 8) {
        this.validaban = false;
        _mensaje = "Contraseña debe tener mínimo 8 caracteres.";
      } else if (!regex.test(pass_usu.trim())) {
        this.validaban = false;
        _mensaje = "La contraseña debe contener al menos un número, una letra en mayúsculas y otro letra minúscula. Contraseña no debe contener espacios.";
      } else if (passc_usu.trim() == "") {
        this.validaban = false;
        _mensaje = "Confirme su contraseña.";
      }else if (pass_usu.trim() != passc_usu.trim()) {
        this.validaban = false;
        _mensaje = "Las contraseñas son distintas.";
      }else if (codigocambio.trim() == "") {
        this.validaban = false;
        _mensaje = "Ingrese código de confirmación.";
    }
    if (!this.validaban) {
      this.validaban = false;
      this.showError(_mensaje, $title)
    }

    return this.validaban;
  }

  validatecambiarclave(pass_act: string, pass_usu: string, passc_usu: string, $title: string) {
    console.log(pass_act)
    console.log(pass_usu)
    console.log(passc_usu)
    let _mensaje = ""
    this.validaban = true;
    const regex : RegExp = /^((?!.*[\s])(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{8,99})/;
    if (pass_act.trim() == "" && pass_usu.trim() == "" && passc_usu.trim() == "") {
      this.validaban = false;
      _mensaje = "Complete los campos para cambiar su contraseña.";
    } else if (pass_act.trim() == "") {
      this.validaban = false;
      _mensaje = "Ingrese su contraseña actual.";
    } else if (pass_usu.trim() == "") {
      this.validaban = false;
      _mensaje = "Ingrese su contraseña nueva.";
    } else if (pass_usu.trim().length < 8) {
      this.validaban = false;
      _mensaje = "Contraseña debe tener mínimo 8 caracteres.";
    } else if (!regex.test(pass_usu.trim())) {
      this.validaban = false;
      _mensaje = "La contraseña debe contener al menos un número, una letra en mayúsculas y otro letra minúscula. Contraseña no debe contener espacios.";
    } else if (passc_usu.trim() == "") {
      this.validaban = false;
      _mensaje = "Confirme su contraseña nueva.";
    } else if (pass_usu.trim() == pass_act.trim()) {
      this.validaban = false;
      _mensaje = "Las contraseñas nueva debe ser distinta a la contraseña actual.";
    } else if (pass_usu.trim() != passc_usu.trim()) {
      this.validaban = false;
      _mensaje = "Las contraseñas son distintas.";
    }

    if (!this.validaban) {
      this.validaban = false;
      this.showError(_mensaje, $title)
    }

    return this.validaban;
  }

  validaPrueba($obj: Prueba): boolean{
    this.validaban = true;
    if($obj.v_cadena.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese cadena.', this.properties.datos_route.prueba.modulo)
    }
    if(!isNumber(parseInt($obj.n_entero.toString()))){
      this.validaban = false;
      this.showError('Ingrese numero entero.', this.properties.datos_route.prueba.modulo)
    }
    if(!isNumber(parseFloat($obj.n_decimal.toString()))){
      this.validaban = false;
      this.showError('Ingrese número decimal.', this.properties.datos_route.prueba.modulo)
    }
    return this.validaban;
  }

  validaTrabajador($obj: Trabajador): boolean{
    this.validaban = true;
    if($obj.v_nombres.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese nombres.', this.properties.datos_route.trabajador.modulo)
    }
    else if(!isNumber(parseInt($obj.v_numdoc.toString()))){
      this.validaban = false;
      this.showError('Ingrese Número de documento.', this.properties.datos_route.trabajador.modulo)
    }

    return this.validaban;
  }

  validaCliente($obj: Cliente): boolean{
    this.validaban = true;
    if($obj.v_numero.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese Numero.', this.properties.datos_route.cliente.modulo)
    }
    else if(!isNumber(parseInt($obj.v_numero.toString()))){
      this.validaban = false;
      this.showError('Ingrese Número de celular.', this.properties.datos_route.cliente.modulo)
    }

    return this.validaban;
  }

  validaConstante($obj: Constante): boolean{
    this.validaban = true;
    if($obj.v_codigo.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese código.', this.properties.datos_route.constante.modulo)
    }
    else if($obj.v_nombre.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese nombre de constante', this.properties.datos_route.constante.modulo)
    }else if($obj.v_descripcion.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese descripción de constante', this.properties.datos_route.constante.modulo)
    }

    return this.validaban;
  }

  validaGeneraUsuario($obj: Trabajador): boolean{
    this.validaban = true;
    if($obj.v_nombres.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese nombres.', this.properties.datos_route.trabajador.modulo)
    }
    else if($obj.v_apellidos.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese apellidos.', this.properties.datos_route.trabajador.modulo)
    }

    return this.validaban;
  }

}
