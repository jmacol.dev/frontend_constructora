import { environment } from 'src/environments/environment';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { MatDialog } from '@angular/material/dialog';
import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';
import { ValidateService } from './validate.service';
import Swal from 'sweetalert2';
import axios from "axios";
import { HeaderautorizaService } from './headerautoriza.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CryptService } from './crypt.service';
import { decode } from 'querystring';
import { NgxSpinnerService } from 'ngx-spinner';
import { Tokens } from '../models/tokens';
import { tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})



export class SeguridadService  {
  public namesystem: string = 'Sistema Integral de Constructora';
  public keyrecaptcha: string = environment.SITEKEY
  public urlexterno: string = environment.URLEXT
  public backgroundlogin: string = 'assets/images/fondologin.png'
  public logologin: string = 'assets/images/logo.png'
  public logomovil: string = 'assets/images/logomovil.png'
  public headerlogin: string = 'assets/images/logohead.png'
  public carpeta: string = 'default'
  public moneda: any = 'S/ '
  public usuarioreg: Usuario = new Usuario();
   ip: string = '0.0.0.0'
   browser: string = ''
   device: string = ''
  public disload: boolean = false;
  constructor( public valserv: ValidateService,
    public hautserv: HeaderautorizaService,
    private router: Router,
    public http: HttpClient,
    private deviceService: DeviceDetectorService,
    private dialogRef: MatDialog,
    private configServ: ConfiguracionService,
    private spinner: NgxSpinnerService,
    public cryptserv: CryptService) {
    this.datos_dispositivo();
  }


  async datos_dispositivo() {
    this.browser =  window.navigator.userAgent
    this.device = this.deviceService.deviceType

    // this.http.get("https://api.ipify.org/?format=json").subscribe((res:any)=>{
    //   this.ip = res.ip
    //   this.browser = navigator.userAgent
    //   this.device = this.deviceService.deviceType
    // });
  }


  async login($usuario: string, $password: string, $recaptchakey: string){
    let $ban: boolean = this.valserv.validatelogin($usuario, $password);

    if ($ban) {
      this.datos_dispositivo();
      let request ={
        usuario: $usuario ?? '',
        password: $password ?? '',
        recaptcha: $recaptchakey ?? '',
        ip: this.ip  ?? '0.0.0.0',
        browser: this.browser,
        device: this.device
      }
      await axios.post(environment.URLAPI + "auth/login", request, {
      headers: this.hautserv.headers,
      })
      .then((response) => {
        this.disload = false;
        if (response.data.estado) {
          this.configServ.textload = 'Iniciando sesión...';
          this.configServ.spinload = 'ball-elastic-dots';
          this.spinner.show();
          console.log(JSON.stringify(response.data.user))
          Swal.fire(this.namesystem, response.data.mensaje, "success").then(()=>{
            response.data.user.password = ''
            let tokens:Tokens = new Tokens();
            tokens.jwt = response.data.token;
            tokens.refreshToken = response.data.token;
            this.doLoginUser(response.data.user,tokens)
            this.router.navigate(['/dashboard']);
          }).then(() => {

            // this.valserv.showInfo(`Hoy están cumpliendo años 1 persona(s).` , this.namesystem)
            this.valserv.showSuccess(response.data.user.trabajador.v_nombres + ' ' + response.data.user.trabajador.v_apellidos , this.namesystem + '\n' + 'Bienvenido al Sistema')
          })

        } else {
          this.removeTokens();
          this.valserv.validaban = false;
          this.valserv.showError(response.data.mensaje, this.namesystem)
        }
      })
      .catch( (error) =>{
        this.disload = false;
        console.log(error);
      }).finally(() => {
        this.spinner.hide();
      });
    }else{
      this.disload = false
    }
  }

  async logout() {
    console.log(localStorage.getItem('token'))
    Swal.fire({
      title: this.namesystem,
      text: '¿Está seguro de cerrar sesión?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        let token = this.cryptserv.get(localStorage.getItem('token')??'')
        axios.post(environment.URLAPI + "auth/logout", {}, {
          headers: this.hautserv.headers_auth(token),
        })
          .then((response) => {
            console.log(JSON.stringify(response))
            this.limpiarsesion()
          })


      }
    })
  }

  private doLoginUser(user: Usuario, tokens: Tokens) {
    localStorage.setItem("user", this.cryptserv.set(JSON.stringify(user)));
    this.storeTokens(tokens);
  }

  public isLoggedIn(): boolean {
    return localStorage.getItem("token") != null && localStorage.getItem("token") != null
  }

  refreshToken() {
    return this.http.post<any>(environment.URLAPI + "auth/refresh-token", {
      'refreshToken': this.getRefreshToken()
    }).pipe(tap((tokens: Tokens) => {
      this.storeJwtToken(tokens.jwt);
    }));
  }

  private storeJwtToken(jwt: string) {
    localStorage.setItem("token", this.cryptserv.set(JSON.stringify(jwt)));
  }

  private storeTokens(tokens: Tokens) {
    localStorage.setItem("token", this.cryptserv.set(JSON.stringify(tokens.jwt)));
    localStorage.setItem("refreshtoken", this.cryptserv.set(JSON.stringify(tokens.refreshToken)));
  }

  private getRefreshToken() {
    return localStorage.getItem("refreshtoken");
  }

  private removeTokens() {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    localStorage.removeItem("refreshtoken");
  }

  limpiarsesion(){
    this.removeTokens()
    this.dialogRef.closeAll();
    this.usuarioreg = new Usuario();
    this.router.navigate(['/login']);

  }



}
