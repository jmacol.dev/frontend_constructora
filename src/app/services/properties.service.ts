import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PropertiesService {

  id_admin = 1

  confirminhab: string = "¿Desea inhabilitar el registro?"
  confirmactiv: string = "¿Desea activar el registro?"
  public prueba = {
    titulo: "Mantenimiento de Pruebas",
    registro: "Registro de Prueba",
    editar: "Editar Prueba",
    listado:  "Listado de Pruebas"
  }

  public trabajador = {
    titulo: "Mantenimiento de trabajador",
    registro: "Registro de trabajador",
    editar: "Editar trabajador",
    listado:  "Listado de trabajadores"
  }

  public cliente = {
    titulo: "Mantenimiento de cliente",
    carga: "Carga de datos de clientes",
    registro: "Registro de cliente",
    editar: "Editar cliente",
    listado:  "Listado de clientes"
  }

  public constante = {
    titulo: "Mantenimiento de constante",
    registrodetalle: "Detalle de constantes",
    registro: "Registro de constante",
    editar: "Editar constante",
    registrotipo: "Registro de tipo constante",
    editartipo: "Editar tipo constante",
    listado:  "Listado de Tipo de Constantes"
  }

  public asignaciontelefonica = {
    titulo: "Mantenimiento de asignacion telefonica",
    registrodetalle: "Detalle de asignacion telefonica",
    registro: "Registro de asignacion telefonica",
    editar: "Editar asignacion telefonica",
    registrotipo: "Registro de tipo asignacion telefonica",
    editartipo: "Editar tipo asignacion telefonica",
    listado:  "Listado de Tipo de asignacion telefonica"
  }

  public resultadoasignacion = {
    titulo: "Gestión de Resultados",
    registro: "Registro de resultados",
    editar: "Editar resultado",
    listado:  "Listado de resultados"
  }

  public datos_route = {
    prueba: {
      route: "prueba",
      modulo: "Mantenimiento de Pruebas"
    },
    cliente: {
      route: "cliente",
      modulo: "Mantenimiento de clientes"
    },
    constante: {
      route: "constante",
      modulo: "Mantenimiento de constantes"
    },
    trabajador: {
      route: "trabajador",
      modulo: "Mantenimiento de trabajadores"
    },

    asignaciontelefonica: {
      route: "asignacion-telefonica",
      modulo: "Mantenimiento de asignacion telefonica"
    },

    detalleasignacion: {
      route: "detalle-asignacion",
      modulo: "Mantenimiento de asignacion telefonica"
    },

    resultadoasignacion: {
      route: "resultado-asignacion",
      modulo: "Resultado de asignación"
    }
  }




  public tipoconstante = {
    tipodocumento: 1,
    estadoasignacion: 4,
    estadocliente: 7,
    resultadoasignacion:16
  }

  public idasesor:number =  15


}
