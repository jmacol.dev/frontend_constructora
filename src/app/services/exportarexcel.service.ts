import { SeguridadService } from 'src/app/services/seguridad.service';
import { Injectable } from '@angular/core';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
@Injectable({
  providedIn: 'root'
})

export class ExportarexcelService {
   numFmtStr = '_("S/"* #,##0.00_);_("S/ "* (#,##0.00);_("S/ "* "-"??_);_(@_)';
  constructor(private segserv: SeguridadService) { }


  exportPrueba(excelData: any) {

    //Title, Header & Data
    const title = excelData.title;
    const header = excelData.headers
    const data = excelData.data;
    const description = excelData.description;

    //Create a workbook with a worksheet
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('Registros');


    //Add Row and formatting
    worksheet.mergeCells('A1', 'H2');
    let titleRow = worksheet.getCell('A1');
    titleRow.value = title
    titleRow.font = {
      name: 'Calibri',
      size: 16,
      underline: 'single',
      bold: true,
      color: { argb: '484a4c' }
    }
    titleRow.alignment = { vertical: 'middle', horizontal: 'center' }

    // Date
    let d = new Date();
    let date = d.getDate().toString().padStart(2,"0") + '-' + (d.getMonth() + 1).toString().padStart(2,"0") + '-' + d.getFullYear();


    //Blank Row
    worksheet.addRow([]);

    //Adding Header Row
    let headerRow = worksheet.addRow(header);
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '484a4c' },
        bgColor: { argb: '' }
      }
      cell.font = {
        bold: true,
        color: { argb: 'FFFFFF' },
        size: 12
      }
    })

    // Adding Data with Conditional Formatting
    data.forEach((d: any) => {
      let row = worksheet.addRow(d);
      let sales = row.getCell(6);
      sales.alignment = { vertical: 'middle', horizontal: 'right' }
      let color = 'a2c692';
      sales.numFmt = this.numFmtStr
      let valor = sales.value!.toString().replace(this.segserv.moneda, '')
      if (valor != null && parseFloat(valor) < 0) {
        color = 'f1a698'
      }

      sales.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: color }
      }
    }
    );
    worksheet.getColumn(1).width = 12;
    worksheet.getColumn(2).width = 15;
    worksheet.getColumn(3).width = 30;
    worksheet.getColumn(4).width = 35;
    worksheet.getColumn(5).width = 12;
    worksheet.getColumn(6).width = 15;
    worksheet.getColumn(7).width = 40;
    worksheet.getColumn(8).width = 80;
    worksheet.addRow([]);

    //Footer Row
    let footerRow = worksheet.addRow(['Listado de ' + title]);
    footerRow.getCell(1).fill = {
      type: 'pattern',
      pattern: 'solid',
      fgColor: { argb: 'ead7da' }
    };

    //Merge Cells
    worksheet.mergeCells(`A${footerRow.number}:H${footerRow.number}`);

    //Generate & Save Excel File
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, title + '.xlsx');
    })

  }
}

