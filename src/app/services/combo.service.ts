import { Injectable } from '@angular/core';
import { ApisService } from './apis.service';
import { PropertiesService } from './properties.service';
import { ConstanteCombo } from '../models/constante';
import { TrabajadorCombo } from '../models/asignacion-telefonica';

@Injectable({
  providedIn: 'root'
})
export class ComboService {
  public comboCategoria: any = [
    { valor: 1, descripcion: 'Opción 1' },
    { valor: 2, descripcion: 'Opción 2' },
    { valor: 3, descripcion: 'Opción 3' },
  ];



  constructor(
    protected apiServ: ApisService
  ) {

  }

  public combo_constante = async (superior: number) => {
    let dataCombo: ConstanteCombo[] = [];
    await this.apiServ.listado(`${this.apiServ.api_constante}select`, `?n_idsuperior=${superior}&n_vigencia=1`).then(response => {
      dataCombo = response;
    });
    return dataCombo;
  }
  public combo_trabajador = async (cargo: number) => {
    let dataCombo: TrabajadorCombo[] = [];
    await this.apiServ.listado(`${this.apiServ.api_trabajador}select`, `?n_idcargo=${cargo}&n_vigencia=1`).then(response => {
      dataCombo = response;
    });
    return dataCombo;
  }
}
