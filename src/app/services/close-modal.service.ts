import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class CloseModalService {

  constructor() { }

  onNoClickDialog($dialogRef: any, opc: number = 0): void {
    $dialogRef.close({ opc: opc });
  }

  cancelarModal(titulo: string, $dialogRef: any) {
    Swal.fire({
          title: titulo,
          text: '¿Desea cancelar el registro?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonText: 'Aceptar',
          cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.onNoClickDialog($dialogRef)
      }
    })
  }
}
