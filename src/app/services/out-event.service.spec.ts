import { TestBed } from '@angular/core/testing';

import { OutEventService } from './out-event.service';

describe('OutEventService', () => {
  let service: OutEventService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OutEventService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
