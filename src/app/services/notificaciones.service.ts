import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificacionesService {

  constructor(private toastr: ToastrService) { }

  showError(msj: string, title: string) {
    this.toastr.error(msj, title);
  }

  showSuccess(msj: string, title: string) {
    this.toastr.success(msj, title);
  }
}
