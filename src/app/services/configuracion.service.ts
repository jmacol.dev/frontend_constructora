import { BreakpointObserver } from '@angular/cdk/layout';
import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { PaginationService } from './pagination.service';
import { MatDrawerMode } from '@angular/material/sidenav';
import { PropertiesService } from './properties.service';
import Swal from 'sweetalert2';
import { ModalService } from './modal.service';
@Injectable({
  providedIn: 'root'
})
export class ConfiguracionService {
  nameSystem: string = "Empresa Constructora"

  public textload = 'Cargando datos...';
  public spinload = 'pacman';


  public datepipe: DatePipe = new DatePipe('en-US')
  public isSmallScreen: any



  constructor(
    public breakpointObserver: BreakpointObserver,
    public properties: PropertiesService,
    public modalServ: ModalService

  ) {
    this.isSmallScreen = breakpointObserver.isMatched('(max-width: 970px)');
  }

  // Funciones para listar combos (<select></select>)




}
