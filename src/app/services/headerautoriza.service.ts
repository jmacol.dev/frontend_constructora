import { Injectable } from '@angular/core';
import { CryptService } from './crypt.service';
import { SeguridadService } from './seguridad.service';
import { SesionService } from './sesion.service';

@Injectable({
  providedIn: 'root'
})
export class HeaderautorizaService {
  public headers = new Headers({
    "Content-Type": "application/json",
    "Access-Control-Allow-Credentials": "true",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET, POST, PATCH, DELETE, PUT, OPTIONS",
    "Access-Control-Allow-Headers":
      "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With",
  });

  constructor(
    public sserv: SesionService
  ) {

  }

  public headers_autdel = async ($token:string) => {
    return {
      "Authorization": `Bearer ${$token}`,
      'Content-Type': 'application/json'
    }
  }

  public headers_formimage = async ($token:string) => {
    return new Headers({
      "Content-Type": "application/x-www-form-urlencoded",
      "Authorization": `Bearer ${$token}`,
      "Access-Control-Allow-Credentials": "true",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PATCH, DELETE, PUT, OPTIONS",
      "Access-Control-Allow-Headers":
        "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With",
    });
  }

  public headers_auth = async ($token:string) => {
    return new Headers({
      "Content-Type": "application/json",
      "Authorization": `Bearer ${$token}`,
      "Access-Control-Allow-Credentials": "true",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PATCH, DELETE, PUT, OPTIONS",
      "Access-Control-Allow-Headers":
        "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With",
    });
  }

}
