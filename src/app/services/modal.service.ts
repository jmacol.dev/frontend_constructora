import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ValidateService } from './validate.service';
import { PruebaMantenimientoComponent } from '../components/modals/prueba-mantenimiento/prueba-mantenimiento.component';
import { PropertiesService } from './properties.service';
import { OutEventService } from './out-event.service';
import { TrabajadorMantenimientoComponent } from '../components/modals/trabajador-mantenimiento/trabajador-mantenimiento.component';
import { ClienteMantenimientoComponent } from '../components/modals/cliente-mantenimiento/cliente-mantenimiento.component';
import { ClienteCargaDatosComponent } from '../components/modals/cliente-carga-datos/cliente-carga-datos.component';
import { ConstanteMantenimientoComponent } from '../components/modals/constante-mantenimiento/constante-mantenimiento.component';
import { ConstanteDetalleComponent } from '../components/modals/constante-detalle/constante-detalle.component';
import Swal from 'sweetalert2';
import { DetalleAsignacion } from '../models/detalle-asignacion';
import { ResultadoAsignacionComponent } from '../components/modals/resultado-asignacion/resultado-asignacion.component';
import { Cliente } from '../models/cliente';
import { ListaResultadosComponent } from '../components/modals/lista-resultados/lista-resultados.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(
    public dialog: MatDialog,
    public valserv: ValidateService,
    public properties: PropertiesService,
    public outServ: OutEventService
  ) { }



  pruebaModal($id: number){
    const dialogRef = this.dialog.open(PruebaMantenimientoComponent, {
      width: '700px',
      panelClass: 'divmodal',
      data: { title: $id == 0 ? this.properties.prueba.registro : this.properties.prueba.editar, id: $id },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((resp) => {
      console.log('The dialog was closed');
      if(resp.opc == 1) this.outServ.listar(null)
    });
  }


  trabajadorModal($id: number){
    const dialogRef = this.dialog.open(TrabajadorMantenimientoComponent, {
      width: '700px',
      panelClass: 'divmodal',
      data: { title: $id == 0 ? this.properties.trabajador.registro : this.properties.trabajador.editar, id: $id },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((resp) => {
      console.log('The dialog was closed');
      if(resp.opc == 1) this.outServ.listar(null)
    });
  }

  clienteModal($id: number){
    const dialogRef = this.dialog.open(ClienteMantenimientoComponent, {
      width: '700px',
      panelClass: 'divmodal',
      data: { title: $id == 0 ? this.properties.cliente.registro : this.properties.cliente.editar, id: $id },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((resp) => {
      console.log('The dialog was closed');
      if(resp.opc == 1) this.outServ.listar(null)
    });
  }

  clienteCargaModal(){
    const dialogRef = this.dialog.open(ClienteCargaDatosComponent, {
      width: '650px',
      panelClass: 'divmodal',
      data: { title: this.properties.cliente.carga},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((resp) => {
      console.log('The dialog was closed');
      if(resp.opc == 1) this.outServ.listar(null)
    });
  }

  constanteModal($id: number, $superior: number) {
    console.log("id ==> " + $id)
    console.log("superior ==> " + $superior)
    let $title: string = $superior == 0 ? ($id == 0 ? this.properties.constante.registrotipo : this.properties.constante.editartipo)
      :($id == 0 ? this.properties.constante.registro : this.properties.constante.editar)
    const dialogRef = this.dialog.open(ConstanteMantenimientoComponent, {
      width: '500px',
      panelClass: 'divmodal',
      data: { title: $title, id:$id, superior:$superior},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((resp) => {
      console.log('The dialog was closed');
      if(resp.opc == 1) this.outServ.listar(null)
    });
  }

  constanteDetalleModal($id: number) {
    const dialogRef = this.dialog.open(ConstanteDetalleComponent, {
      width: '800px',
      panelClass: 'divmodal',
      data: { title: this.properties.constante.registrodetalle, id:$id},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
      this.outServ.listar(null)
    });
  }

  resultadoAsignacionModal($id: number, $data: DetalleAsignacion) {
    const dialogRef = this.dialog.open(ResultadoAsignacionComponent, {
      width: '600px',
      panelClass: 'divmodal',
      data: { title: this.properties.resultadoasignacion.registro, id:$id, data: $data},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((resp) => {
      console.log('The dialog was closed' + resp.opc);
      if(resp.opc == 1) this.outServ.listar(null)
    });
  }

  listaResultadosModal($id: number, $data: Cliente) {
    const dialogRef = this.dialog.open(ListaResultadosComponent, {
      width: '800px',
      panelClass: 'divmodal',
      data: { title: this.properties.resultadoasignacion.listado, id:$id, data: $data},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
      // this.outServ.listar(null)
    });
  }

}
