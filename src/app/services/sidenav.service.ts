import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { Injectable } from '@angular/core';
import { MatDrawerMode, MatSidenav, MatSidenavContent } from '@angular/material/sidenav';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {
  public iconmenu: boolean = true;
  public modesidenav: MatDrawerMode = "push" as MatDrawerMode
  constructor(

  ) { }

  public toogle(toogle: any){
    return toogle;
  }

  public abrirmenu($sideNav: any){
    this.toogle = () => {
      console.log("entrar " + this.modesidenav)
      $sideNav.iconmenu = ($sideNav.iconmenu ? false : true);
      this.iconmenu = ($sideNav.iconmenu ? false : true);
      $sideNav.mode = this.modesidenav
      $sideNav.toggle();
    };
  }
}
