import { Injectable } from '@angular/core';
import { MatPaginatorIntl, PageEvent } from '@angular/material/paginator';

@Injectable({
  providedIn: 'root'
})
export class PaginationModalService {
  dataSourceModal: any[] = []
  page: number = 0;
  pageSize: number = 5;
  pageSizeOptions = [5,10,15,20];
  coleccionsize: number =0
  constructor(private paginator: MatPaginatorIntl) {
    this.paginator.itemsPerPageLabel = "Items por página";
  }

  inicializarModal():PageEvent {
    let pageEvent: PageEvent = new PageEvent
    pageEvent.length = 0
    pageEvent.pageIndex = 0
    pageEvent.pageSize = 5
    return pageEvent;
  }

  public actualizaTablaModal = ($data: any, $event:PageEvent) => {
      this.pageSize = $event.pageSize;
      this.page = $event.pageIndex
      this.dataSourceModal = $data
      .map((item: any, i: number) => ({row: i + 1, ...item}))
      .slice((this.page) * this.pageSize, (this.page) * this.pageSize + this.pageSize);
    return  this.dataSourceModal;
  }
}
