import { Trabajador } from './../models/trabajador';
import { Cliente } from './../models/cliente';
import { Injectable, OnInit } from '@angular/core';
import { ValidateService } from './validate.service';
import { HeaderautorizaService } from './headerautoriza.service';
import { SesionService } from './sesion.service';
import Swal from 'sweetalert2';
import axios, { AxiosRequestConfig } from "axios";
import { environment } from "./../../environments/environment";
import { SeguridadService } from './seguridad.service';
import { PropertiesService } from './properties.service';
@Injectable({
  providedIn: 'root'
})
export class ApisService{
  ip: string = '0.0.0.0'
  token: string = ''
  api_constante!:string;
  api_trabajador!:string;
  api_cliente!: string;
  api_asignatelefono!: string;
  api_detalleasigna!: string;
  api_prueba!: string;
  api_resultadoasignacion !: string
  constructor(public valserv: ValidateService,
    public hautserv: HeaderautorizaService,
    public sserv: SesionService,
    protected properties: PropertiesService) {

    this.token = this.sserv.gettoken((localStorage.getItem('token') ?? '')).toString()
    this.api_constante = `${properties.datos_route.constante.route}/`
    this.api_trabajador = `${properties.datos_route.trabajador.route}/`
    this.api_cliente = `${properties.datos_route.cliente.route}/`
    this.api_asignatelefono = `${properties.datos_route.asignaciontelefonica.route}/`
    this.api_detalleasigna = `${properties.datos_route.detalleasignacion.route}/`
    this.api_resultadoasignacion = `${properties.datos_route.resultadoasignacion.route}/`
    this.api_prueba = `${properties.datos_route.prueba.route}/`

    this.obtener_ip()
  }

  obtener_ip() {
    axios.get("https://api.ipify.org/?format=json").then((res:any)=>{
      this.ip = res.ip
    });
  }

  public registro = async ($title: string, $route: string, resquest: any) => {
    console.log(JSON.stringify(resquest))
    let $dataret: boolean = true;
      resquest.a_v_ipcrea = this.ip;
      await axios.post(environment.URLAPI + $route, resquest, {
      headers: this.hautserv.headers_auth(this.token),
      })
        .then((response) => {
        console.log(JSON.stringify(response))
        if (response.data.estado) {
          $dataret = true;
          Swal.fire($title, response.data.mensaje, response.data.codigo == 1 ? "success" : "info");
        }else{
          this.valserv.validaban = false
          this.valserv.showError(response.data.mensaje, $title)
          $dataret = false;
        }
      })
      .catch( (error) =>{
        $dataret = false;
        this.valserv.validaban = false
        this.valserv.showError(error, $title)
        // console.log(error);
      });
    return $dataret;
  }

  public actualizar = async ($title: string, $route: string, resquest: any) => {
    let $dataret: boolean = true;
    resquest.a_v_ipmodif = this.ip;
    console.log("datos actualizar ===> " +JSON.stringify(resquest))
      await axios.put(environment.URLAPI + $route, resquest, {
      headers: this.hautserv.headers_auth(this.token),
      })
      .then((response) => {
        if (response.data.estado) {
          $dataret = true;
          Swal.fire($title, response.data.mensaje, response.data.codigo == 1 ? "success" : "info");
        }else{
          this.valserv.validaban = false
          this.valserv.showError(response.data.mensaje, $title)
          $dataret = false;
        }
      })
      .catch( (error) =>{
        $dataret = false;
        this.valserv.validaban = false
        this.valserv.showError(error, $title)
        console.log(error);
      });
    return $dataret;
  }

  public estado = async ($title: string, $route: string, resquest: any) => {
    let $dataret: boolean = true;
    resquest.a_v_ipmodif = this.ip;
    console.log("api ===> " + environment.URLAPI + $route)
    console.log("delete ===> " + JSON.stringify(resquest))
    await axios.delete(environment.URLAPI + $route, {
        data:  resquest ,
        headers: this.hautserv.headers_autdel(this.token)
      })
      .then((response) => {
        if (response.data.estado) {
          $dataret = true;
          Swal.fire($title, response.data.mensaje, response.data.codigo == 1 ? "success" : "info");
        }else{
          this.valserv.validaban = false
          this.valserv.showError(response.data.mensaje, $title)
          $dataret = false;
        }
      })
      .catch( (error) =>{
        $dataret = false;
        this.valserv.validaban = false
        this.valserv.showError(error, $title)
        console.log(error);
      });
    return $dataret;
  }

  public listado = async  ($route: string, resquest: any)  => {

    let $data: any = [];
    let options: AxiosRequestConfig = {
      params: {},
      headers: {
        'Authorization': 'Bearer ' + this.token,
        'Content-Type': 'application/json'
      }
    }
    console.log(JSON.stringify(options))
    await axios.get(environment.URLAPI + $route + resquest, options)
      .then((response) => {
        console.log(JSON.stringify(response))
        if (response.data.estado) {
          $data = response.data.data;
        } else {
          this.valserv.validaban = true
          this.valserv.showError(response.data.mensaje, "Mensaje de validaciones")
        }
      })
      .catch( (error) =>{
        console.log(error);
      });
      return $data;
  }
}
