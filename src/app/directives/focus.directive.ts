import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appFocus]'
})
export class FocusDirective {


  constructor(private readonly elRef: ElementRef) { }

  @HostListener('focus')onFocus(){
    this.elRef.nativeElement.style.backgroundColor = '#F8F3D7';
    this.elRef.nativeElement.style.padding = '10px'
  }

  @HostListener('blur')onBlur(){
    this.elRef.nativeElement.style.backgroundColor = 'white';
    this.elRef.nativeElement.style.padding = '0px'
  }

}
