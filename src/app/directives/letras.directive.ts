import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appLetras]'
})
export class LetrasDirective {

  // Allow decimal numbers and negative values
  private regex: RegExp = new RegExp(/^[A-Za-záéíóúÁÉÍÓÚ\s]+$/g);
  // Allow key codes for special events. Reflect :/^([0-9])*$/
  // Backspace, tab, end, home
  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', '-', 'ArrowLeft', 'ArrowRight', 'Del', 'Delete'];

  constructor(private el: ElementRef) {
  }
  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    // Allow Backspace, tab, end, and home keys
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    let current: string = this.el.nativeElement.value;
    const position = this.el.nativeElement.selectionStart;
    const next: string = [current.slice(0, position), event.key == 'Decimal' ? '.' : event.key, current.slice(position)].join('');
    if (next && !String(next).match(this.regex)) {
      event.preventDefault();
    }
  }

}
