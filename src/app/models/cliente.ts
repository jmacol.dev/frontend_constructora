import { Base } from "./base";
import { Constante } from "./constante";

export class Cliente extends Base {
  n_idcliente: number = 0;
  v_numero: string = '';
  v_dni: string = '';
  v_nombre: string = '';
  n_idestadocliente: number = 0;
  tipoestado: Constante = new Constante();
}


export class ClienteCarga{
  numero: string = "";
  fecha: string = "";

  constructor(p_numero:string, p_fecha:string) {
    this.numero = p_numero;
    this.fecha = p_fecha;
  }
}
