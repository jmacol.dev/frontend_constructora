import { Base } from "./base";
import { Constante } from "./constante";

export class ResultadoAsignacion extends Base {
  n_idresultado: number = 0;
  d_fecharesultado: Date = new Date();
  v_observaciones: string = ''
  n_idasignadeta: number = 0;
  n_idestadoresultado: number = 0;
  estadoresultado:Constante = new Constante()
}
