import { Base } from "./base";
import { Trabajador } from "./trabajador";

export class Usuario extends Base {
  n_idusuario: number = 0;
  usuario: string = '';
  password: string = '';
  v_codigoreset: string = '';
  d_fechareset: Date = new Date();
  n_idtrabajador:number = 0;
  trabajador: Trabajador = new Trabajador();
}






