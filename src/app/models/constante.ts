import { Base } from "./base";

export class Constante extends Base {
  n_idconstante: number = 0;
  v_codigo: string = '';
  v_nombre: string = '';
  v_descripcion: string = '';
  v_valor: string = '';
  n_idsuperior: any = 0;
}

export class ConstanteCombo {
  n_idconstante: number = 0;
  v_nombre: string = "";
}
