import { Base } from "./base";

export class Prueba extends Base{
  n_id: number = 0;
  v_cadena: string = "";
  n_entero: number = 0;
  n_decimal: number = 0.0;
  d_fecha: Date = new Date();
  n_combo: number = 0;
  n_autocomplete: number = 0;
}

