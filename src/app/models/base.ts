export class Base {
  b_vigencia: number = 1;
  a_v_usuariocrea: string = "";
  a_d_fechacrea: Date = new Date();
  a_v_ipcrea: string = "0.0.0.0";
  a_v_usuariomodif: string = "";
  a_d_fechamodif: Date= new Date();
  a_v_ipmodif: string = "0.0.0.0";
  a_d_fechabaja: Date= new Date();
}
