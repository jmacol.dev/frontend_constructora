import { Base } from "./base";
import { Constante } from "./constante";
//import { Usuario } from "./usuario";

export class Trabajador extends Base {
  n_idtrabajador: number = 0;
  v_numdoc: string = "";
  v_nombres: string = "";
  v_apellidos: string = "";
  v_telefono: string = "";
  v_email: string = "";
  n_idtipodoc: number = 0;
  tipodocumento: Constante = new Constante();
  //usuario: Usuario=new Usuario();
}
