import { Base } from "./base";
import { Constante } from "./constante";
import { Trabajador } from "./trabajador";

export class AsignacionTelefonica extends Base{
  n_idasignacion: number = 0;
  d_fechaasignacion: Date = new Date();
  n_idtrabajador: number = 0;
  n_idcliente: number = 0;
  estadoasignacion: Constante = new Constante();
  trabajador: Trabajador =  new Trabajador()

  }

  export class TrabajadorCombo {
    n_idtrabajador: number = 0;
    trabajador: string = "";
    
  }

  export class ConstanteCombo {
    n_idconstante: number = 0;
    v_nombre: string = "";
  }
  
