import { Base } from "./base";
import { Cliente } from "./cliente";
import { Constante } from "./constante";

export class DetalleAsignacion extends Base{
  n_idasignadeta: number = 0;
  n_idcliente: number = 0
  d_fechavisita!: Date;
  n_idultimoresultado!: number;
  n_idasignacion: number = 0;
  cliente: Cliente = new Cliente();
  ultimoresultado !:Constante
}
