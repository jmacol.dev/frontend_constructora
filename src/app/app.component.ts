import { Component, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { Console } from 'console';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { SeguridadService } from './services/seguridad.service';
import { SesionService } from './services/sesion.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'systemConstructora';

  userActivity:any;
  userInactive: Subject<any> = new Subject();

  constructor(
    private router: Router,
    public segserv: SeguridadService
  ) {
    console.log(location.hostname)
    // var styles = "https://"+segserv.urlexterno+"/styles/"+segserv.carpeta+"/style.scss";
    // var newSS=document.createElement('link');
    // newSS.rel='stylesheet';
    // newSS.href=unescape(styles);
    // document.getElementsByTagName("head")[0].appendChild(newSS);
    // this.setTimeout();
    this.userInactive.subscribe(() => {
      if (localStorage.getItem('token') !== '' && localStorage.getItem('token') !== null) {
      // if(localStorage.getItem("token") !== '' && localStorage.getItem("token") !== null){
        this.segserv.limpiarsesion()
        Swal.fire(this.segserv.namesystem, "Por seguridad el sistema ha cerrado sesión, dado que no hubo actividad.", "info")
      }
    });
    // document.oncontextmenu = function () {
    //   // alert("no tiene permiso")
    //   return false;
    // }
  }

  // setTimeout() {
  //   this.userActivity = setTimeout(() => this.userInactive.next(undefined), (1000 * 60 * 10));
  // }

  // @HostListener('window:mousemove') refreshUserState() {
  //   clearTimeout(this.userActivity);
  //   this.setTimeout();
  // }
}
