import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { SesionService } from '../services/sesion.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    public sesionServ: SesionService,
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
     request = request.clone({
        setHeaders: {
            Authorization: `Bearer ` + this.sesionServ.gettoken(localStorage.getItem('token') ?? "").toString()
        }
    });
    return next.handle(request);
  }
}
