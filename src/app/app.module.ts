import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Router, RouterModule } from '@angular/router';
import { LoginComponent } from './components/auth/login/login.component';
import { DashboardComponent } from './components/home/dashboard/dashboard.component';
import { HeaderComponent } from './components/template/header/header.component';
import { MenuComponent } from './components/template/menu/menu.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { RECAPTCHA_SETTINGS, RecaptchaModule, RecaptchaSettings } from "ng-recaptcha";
import {  MatTableModule } from '@angular/material/table';
import {  MatSidenavModule} from '@angular/material/sidenav';
import {  MatToolbarModule} from '@angular/material/toolbar';
import {  MatIconModule} from '@angular/material/icon';
import {  MatFormFieldModule, MatLabel} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import {  MatInputModule} from '@angular/material/input';
import { MatExpansionModule} from '@angular/material/expansion';
import {  MatAutocompleteModule} from '@angular/material/autocomplete';
import {  MatDatepickerModule, MatDatepickerToggle} from '@angular/material/datepicker';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {  MatDialogModule,  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatChipsModule} from '@angular/material/chips';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MatRadioModule} from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ButtonComponent } from './components/template/button/button.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatNativeDateModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { TitlemoduleComponent } from './components/template/titlemodule/titlemodule.component';
import { CambiarpassComponent } from './components/auth/cambiarpass/cambiarpass.component';
import { DecimalesDirective } from './directives/decimales.directive';
import { LetrasDirective } from './directives/letras.directive';
import { CryptService } from './services/crypt.service';
import { MatCheckboxModule } from '@angular/material/checkbox';
import axios from 'axios';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { SoloNumerosDirective } from './directives/solo-numeros.directive';
import { FocusDirective } from './directives/focus.directive';
import { PruebaComponent } from './components/pages/prueba/prueba.component';
import { PruebaMantenimientoComponent } from './components/modals/prueba-mantenimiento/prueba-mantenimiento.component';
import { environment } from 'src/environments/environment';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { ActivoGuard } from './guards/activo.guard';
import { ClienteComponent } from './components/pages/cliente/cliente.component';
import { LoadingComponent } from './components/template/loading/loading.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { OlvideClaveComponent } from './components/auth/olvide-clave/olvide-clave.component';
import { RecuperarClaveComponent } from './components/auth/recuperar-clave/recuperar-clave.component';
import { TrabajadorMantenimientoComponent } from './components/modals/trabajador-mantenimiento/trabajador-mantenimiento.component';
import { TrabajadorComponent } from './components/pages/trabajador/trabajador.component';
import { ClienteCargaDatosComponent } from './components/modals/cliente-carga-datos/cliente-carga-datos.component';
import { ConstanteComponent } from './components/pages/constante/constante.component';
import { ConstanteMantenimientoComponent } from './components/modals/constante-mantenimiento/constante-mantenimiento.component';
import { ClienteMantenimientoComponent } from './components/modals/cliente-mantenimiento/cliente-mantenimiento.component';
import { ConstanteDetalleComponent } from './components/modals/constante-detalle/constante-detalle.component';
import { InactivoGuard } from './guards/inactivo.guard';
import { SeguridadService } from './services/seguridad.service';
import { AsignacionTelefonicaComponent } from './components/pages/asignacion-telefonica/asignacion-telefonica.component';
import { ResultadoAsignacionComponent } from './components/modals/resultado-asignacion/resultado-asignacion.component';
import { ListaResultadosComponent } from './components/modals/lista-resultados/lista-resultados.component';
// import { CookieService } from 'ngx-cookie-service';
// import { MatMomentDateModule } from "@angular/material-moment-adapter";

@NgModule({
  declarations: [
    // ComponentesPropios
    AppComponent,
    LoginComponent,
    DashboardComponent,
    HeaderComponent,
    MenuComponent,
    ButtonComponent,
    TitlemoduleComponent,
    CambiarpassComponent,
    DecimalesDirective,
    LetrasDirective,
    SoloNumerosDirective,
    FocusDirective,
    PruebaComponent,
    PruebaMantenimientoComponent,
    ClienteComponent,
    LoadingComponent,
    OlvideClaveComponent,
    RecuperarClaveComponent,
    TrabajadorMantenimientoComponent,
    TrabajadorComponent,
    ClienteCargaDatosComponent,
    ConstanteComponent,
    ConstanteMantenimientoComponent,
    ClienteMantenimientoComponent,
    ConstanteDetalleComponent,
   AsignacionTelefonicaComponent,
   ResultadoAsignacionComponent,
   ListaResultadosComponent

    // ComponentesAngular

  ],
  entryComponents: [
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatTableModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatDialogModule,
    MatChipsModule,
    NgbModule,
    FormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatListModule,
    MatTooltipModule,
    MatMenuModule,
    MatSelectModule,
    MatCardModule,
    MatProgressSpinnerModule,
    RecaptchaModule,
    MatPaginatorModule,
    HttpClientModule,
    MatChipsModule,
    MatRadioModule,
    ToastrModule.forRoot({
      timeOut: 15000,
      positionClass :'toast-bottom-right'
    }),
    NgxSpinnerModule
    //  MatMomentDateModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: MatDialogRef,    useValue: []},
    { provide: MAT_DIALOG_DATA, useValue: [] },
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: { siteKey: environment.SITEKEY } as RecaptchaSettings,
    },
    { provide: MAT_DATE_LOCALE, useValue: 'es-GB' },  { provide: ToastrService }, CryptService, SeguridadService, ActivoGuard, InactivoGuard
    ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule { }
